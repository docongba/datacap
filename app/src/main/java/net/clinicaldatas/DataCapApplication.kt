package net.clinicaldatas

import android.app.Application
//import android.arch.persistence.room.Room
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
//import net.clinicaldatas.database.DataCapDatabase
import net.clinicaldatas.utils.TypefaceUtil
import io.realm.Realm
import io.realm.RealmConfiguration
import net.clinicaldatas.R.style.TextField
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

/**
 * Created by doba on 1/25/18.
 */
class DataCapApplication : MultiDexApplication() {
//    override fun onCreate() {
//        super.onCreate()
//
//        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Roboto-Regular.ttf");
//    }

    companion object {
//        var database: DataCapDatabase? = null
    }

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this);

//        var c = RealmConfiguration.Builder(applicationContext)
        Realm.init(this)
        val config = RealmConfiguration.Builder().name("DataCap.realm").build()
        Realm.setDefaultConfiguration(config)
//        c.name("DataCap")
//        c.deleteRealmIfMigrationNeeded()
//        Realm.setDefaultConfiguration(c.build())
    }
}