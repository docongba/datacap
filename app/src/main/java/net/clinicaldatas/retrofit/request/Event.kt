package net.clinicaldatas.retrofit.request

import com.google.gson.annotations.SerializedName
import net.clinicaldatas.database.DataValueDbO

/**
 * Created by doba on 2/5/18.
 */
class Event{

    /**event id*/

    @SerializedName("programStage")
    var programStage : String = "";

    @SerializedName("dueDate")
    var dueDate  : String = "";

    @SerializedName("status")
    var status  : String = "";

    @SerializedName("eventDate")
    var eventDate : String = "";

    @SerializedName("dataValues")
    var dataValues = ArrayList<DataValue>();
}