package net.clinicaldatas.retrofit.request

import com.google.gson.annotations.SerializedName
import net.clinicaldatas.database.EventDbO

/**
 * Created by doba on 2/3/18.
 */
class SyncPostData{

    @SerializedName("orgUnit")
    var orgUnit : String = "";

    //Study
    @SerializedName("program")
    var program : String  = "";

    @SerializedName("trackedEntityInstance")
    var trackedEntityInstance = TrackedEntityItem();

    @SerializedName("enrollment")
    var enrollment = Enrollment();

    @SerializedName("events")
    var events = ArrayList<Event>();

    class TrackedEntityItem{
        @SerializedName("trackedEntity")
        var trackedEntity : String = ""

        @SerializedName("attributes")
        var attributes  = ArrayList<Attribute>();

    }

    class Attribute{
        @SerializedName("attribute")
        var attribute : String = ""

        @SerializedName("value")
        var value : String = ""
    }

    class Enrollment{
        @SerializedName("enrollmentDate")
        var enrollmentDate : String = ""

        @SerializedName("incidentDate")
        var incidentDate : String = ""

        @SerializedName("status")
        var status : String = "";
    }




}
