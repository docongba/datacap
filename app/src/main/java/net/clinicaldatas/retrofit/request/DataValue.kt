package net.clinicaldatas.retrofit.request

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

/**
 * Created by doba on 2/5/18.
 */
class DataValue{

    /* id of Question*/
    @SerializedName("dataElement")
    var dataElement : String = "";

    @SerializedName("value")
    var value : String = ""

    @SerializedName("rowNum")
    var rowNum : String = ""
}