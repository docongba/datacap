package net.clinicaldatas.retrofit.response

import net.clinicaldatas.models.OrganizeItem
import net.clinicaldatas.models.ProgramItem


/**
 * Created by doba on 1/30/18.
 */
class LoginResponse{
    lateinit var name : String;
    lateinit var surname : String;
    lateinit var email : String;
    lateinit var firstName : String;
    lateinit var displayName : String;
    lateinit var id : String;


    lateinit var userCredentials : UserCredential;
    lateinit var organisationUnits : ArrayList<OrganizeItem>;


    inner class UserCredential{
        lateinit var userRoles : ArrayList<UserRole>;
    }

    inner class UserRole{

        lateinit var programs : ArrayList<ProgramItem>;
    }
}