package net.clinicaldatas.retrofit.response

import net.clinicaldatas.models.ProgramItem

/**
 * Created by doba on 1/30/18.
 */
class MetadataResponse {

    lateinit var programs : ArrayList<ProgramItem>;
}