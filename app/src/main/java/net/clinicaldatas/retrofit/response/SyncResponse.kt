package net.clinicaldatas.retrofit.response

/**
 * Created by doba on 2/21/18.
 */
class SyncResponse {
    var httpStatus : String = "";
    var message : String = "";
    var httpStatusCode : Int = 0

}