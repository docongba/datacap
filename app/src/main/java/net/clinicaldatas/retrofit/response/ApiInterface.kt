package net.clinicaldatas.retrofit.response

import android.text.TextUtils
import net.clinicaldatas.BuildConfig
import net.clinicaldatas.retrofit.response.reposity.AuthenticationInterceptor
import net.clinicaldatas.utils.APIConstant
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import android.util.Base64
import net.clinicaldatas.retrofit.request.SyncPostData
import retrofit2.http.*


/**
 * Created by doba on 1/31/18.
 */
interface  ApiInterface {

    @GET(APIConstant.LOGIN)
    fun login() : Call<LoginResponse>;

    @GET
    fun getMetadata(@Url url : String) : Call<MetadataResponse>;

    @POST(APIConstant.SYNC_DATA)
    fun postSyncData(@Body data : SyncPostData) : Call<SyncResponse>

    companion object Factory {

        private val builder = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(APIConstant.BASE_API)

        private var retrofit = builder.build()
        private val httpClient = OkHttpClient.Builder()

//        fun <S> createService(serviceClass: Class<S>): S {
//            return createService(serviceClass, null, null)
//        }

        fun <S> createService(
                serviceClass: Class<S>, username: String?, password: String?): S {
            if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
//                val authToken = Credentials.basic(username, password)

                val credentials = username + ":" + password
                val auth = "Basic " + Base64.encodeToString(credentials.toByteArray(),
                        Base64.NO_WRAP)

                return createService(serviceClass, auth)
            }

            return createService(serviceClass, null)
        }

        fun <S> createService(
                serviceClass: Class<S>, authToken: String?): S {
            if (!TextUtils.isEmpty(authToken)) {
                val interceptor = AuthenticationInterceptor(authToken!!)

                if (!httpClient.interceptors().contains(interceptor)) {
                    httpClient.addInterceptor(interceptor)
                            .addInterceptor(HttpLoggingInterceptor().apply {
                                level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
                            })

                    builder.client(httpClient.build())
                    retrofit = builder.build()
                }
            }

            return retrofit.create(serviceClass)
        }
    }


}