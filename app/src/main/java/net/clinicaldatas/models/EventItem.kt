package net.clinicaldatas.models

import java.io.Serializable

/**
 * Created by doba on 1/21/18.
 */
class EventItem : Serializable{
    var subId : String = ""
    var name: String = "";
    var description : String = "";
    var id : String = "";
    var selected : Boolean = false;
    var programStageDataElements = ArrayList<ProgramStageDataElement>();
    lateinit var programStageSections : ArrayList<FormItem>;

    inner class ProgramStageDataElement{
        var compulsory : Boolean = false;
        lateinit var dataElement : DataElement;
    }

    inner class DataElement{
        var id : String = ""
    }
}