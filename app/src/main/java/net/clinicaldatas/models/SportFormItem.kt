package net.clinicaldatas.models

import android.util.Log
import io.realm.RealmObject
import io.realm.annotations.Ignore

/**
 * Created by doba on 3/17/18.
 */
open class SportFormItem : RealmObject(){

    @Ignore
    var questionList = ArrayList<QuestionItem>();

    var formId : String = ""
    var subId : String = ""
    var eventId : String = ""
    var sportFormId : String = ""
    var firstChoice : String = ""
    var secondChoice : String = ""
    var thirdChoice : String = ""
    var fourthChoice : String = ""
    var fifthChoice : String = ""

    fun isPassed() : Boolean{
        if(firstChoice.isNotEmpty() && secondChoice.isNotEmpty() && thirdChoice.isNotEmpty() && fourthChoice.isNotEmpty() && fifthChoice.isNotEmpty()){
            return true
        }

        return false

    }

    fun isPassedShort() : Boolean{
        if(firstChoice.isNotEmpty() && secondChoice.isNotEmpty() && thirdChoice.isNotEmpty()){
            return true
        }

        return false
    }
}