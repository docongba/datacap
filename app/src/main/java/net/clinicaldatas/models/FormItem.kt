package net.clinicaldatas.models

import java.io.Serializable

/**
 * Created by doba on 1/21/18.
 */

class FormItem : Serializable{
    var subId : String = ""
    var displayName : String = "";
    var id : String = "";
    var selected : Boolean = false;
    var index : Int = 0;
    var completed : Boolean = false

    var groupForm : Boolean = false

    var sortOrder : Int = 0

    var dataElements : ArrayList<QuestionItem> = ArrayList<QuestionItem>();
    var programStageSubSections : ArrayList<ProgramStageSubSection>  = ArrayList<ProgramStageSubSection>();
}
