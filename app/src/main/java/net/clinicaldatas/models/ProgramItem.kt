package net.clinicaldatas.models

import com.google.gson.annotations.SerializedName

/**
 * Created by doba on 1/21/18.
 */
class ProgramItem {
    var name: String = "";
    var description : String = ""
    var date : String = "";
    var id : String = "";
    var trackedEntity = TrackedEntity();

    lateinit var programStages : ArrayList<EventItem> ;
    lateinit var programTrackedEntityAttributes : ArrayList<ProgramTrackedEntity>;


    inner class ProgramTrackedEntity{
        var displayName : String = ""
        var id : String = "";

        lateinit var trackedEntityAttribute : TrackedEntityAttribute;
    }

    inner class TrackedEntityAttribute{
        var name : String =""
        var id : String = ""
    }

    inner class TrackedEntity{
        var displayName : String = ""
        var id : String = ""
    }


}