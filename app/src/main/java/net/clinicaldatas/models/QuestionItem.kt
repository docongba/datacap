package net.clinicaldatas.models

import io.realm.RealmObject
import java.io.Serializable

/**
 * Created by doba on 1/23/18.
 */


class QuestionItem : Serializable{
    var id : String = "";
    var displayName : String = "";
    var valueType : String = "";
    var optionSetValue : Boolean = false;
    var displayFormName : String = "";
    lateinit var optionSet : OptionSet;
    var compulsory : Boolean = false
    var url : String = "";

    class OptionSet{
        var options = ArrayList<AnswerItem>();
    }
}