package net.clinicaldatas.models

/**
 * Created by doba on 2/12/18.
 */
class ProgramStageSubSection{
    var id : String = ""
    var displayName : String  = ""
    var dataElements : ArrayList<QuestionItem> = ArrayList<QuestionItem>();
}