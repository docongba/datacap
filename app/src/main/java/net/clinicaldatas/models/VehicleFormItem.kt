package net.clinicaldatas.models

import io.realm.RealmObject
import io.realm.annotations.Ignore

/**
 * Created by doba on 3/17/18.
 */
open class VehicleFormItem : RealmObject(){

    var formId : String = ""
    var subId : String = ""
    var eventId : String = ""
    var vehicleFormId : String = ""
    var vehicleName : String = ""
    var monChoice : Boolean = false
    var tueChoice : Boolean = false
    var wedChoice : Boolean = false
    var thurChoice : Boolean = false
    var friChoice : Boolean = false
    var satChoice : Boolean = false
    var sunChoice : Boolean = false

    var total : Int = 0

    @Ignore
    var questionList = ArrayList<QuestionItem>();

}