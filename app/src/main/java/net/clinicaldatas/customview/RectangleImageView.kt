package net.clinicaldatas.customview

import android.content.Context
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import android.widget.ImageView

/**
 * Created by doba on 1/22/18.
 */
class RectangleImageView : AppCompatImageView {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {}

    public override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec) // This is the key that will make the height equivalent to its width
        val newHeight = widthMeasureSpec * 3 / 4
        setMeasuredDimension(widthMeasureSpec, newHeight)

    }
}
