package net.clinicaldatas.`interface`

import net.clinicaldatas.fragment.QuestionCategoryFragment

/**
 * Created by doba on 2/5/18.
 */
interface OnboardingCateFragmentDelegate {
    fun onQuestionCateFragmentNext(fragment: QuestionCategoryFragment)
    fun onQuestionCateFragmentBack(fragment: QuestionCategoryFragment)
}