package net.clinicaldatas.`interface`

import net.clinicaldatas.activity.QuestionActivity
import net.clinicaldatas.fragment.QuestionCategoryFragment
import net.clinicaldatas.fragment.QuestionFragment

/**
 * Created by doba on 2/5/18.
 */
interface OnboardingFragmentDelegate{
    fun onQuestionFragmentNext(fragment: QuestionFragment)
    fun onQuestionFragmentBack(fragment: QuestionFragment)
}

