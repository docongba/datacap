package net.clinicaldatas.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import net.clinicaldatas.R
import net.clinicaldatas.models.FormItem
import net.clinicaldatas.utils.DataSingleton
import kotlinx.android.synthetic.main.row_form.view.*
import net.clinicaldatas.database.DatabaseUtils

/**
 * Created by doba on 1/21/18.
 */

class FormAdapter(private val context : Context, private val formsDatas: ArrayList<FormItem>,
                  var onItemClick: (Int) -> Unit) :
        RecyclerView.Adapter<FormAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_form, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(formsDatas[position], position)
    }

    override fun getItemCount() = formsDatas.size

    inner class ViewHolder(view: View)
        : RecyclerView.ViewHolder(view) {

        fun bindData(formItem: FormItem, position: Int) {
            with(formItem) {
                itemView.tv_form_name.text = formItem.displayName;
                itemView.setOnClickListener { onItemClick(position) }

                if(formItem.selected){
                    itemView.tv_form_name.setCompoundDrawablesWithIntrinsicBounds(context.resources.getDrawable(R.drawable.ic_desciption_white), null, null, null);
                    itemView.rl_bg_event.setBackgroundResource(R.drawable.bg_event_selected);
                    itemView.tv_form_name.setTextColor(context.resources.getColor(R.color.white))
                    itemView.tv_progress.setTextColor(context.resources.getColor(R.color.white))
                }else{
                    itemView.tv_form_name.setCompoundDrawablesWithIntrinsicBounds(context.resources.getDrawable(R.drawable.ic_description_normal), null, null, null);
                    itemView.rl_bg_event.setBackgroundResource(R.drawable.bg_event);
                    itemView.tv_form_name.setTextColor(context.resources.getColor(R.color.light_gray))
                    itemView.tv_progress.setTextColor(context.resources.getColor(R.color.light_gray))
                }

                if(formItem.groupForm){
                    itemView.tv_progress.setText(DatabaseUtils.getAllSportGroupForm(formItem.id, DataSingleton.instance.currentSubId).size.toString());
                }else if(formItem.programStageSubSections.size > 0){

                    var totalSize = 0

                    for(i in 0..formItem.programStageSubSections.size - 1){
                        totalSize = totalSize + formItem.programStageSubSections.get(i).dataElements.size
                    }

                    itemView.tv_progress.setText(DatabaseUtils.getAllQuestionsByForm(formItem.id, DataSingleton.instance.currentSubId).count().toString() + "/" + totalSize);
                }else{
                    itemView.tv_progress.setText(DatabaseUtils.getAllQuestionsByForm(formItem.id, DataSingleton.instance.currentSubId).count().toString() + "/" + formItem.dataElements.size);
                }
            }
        }
    }
}

