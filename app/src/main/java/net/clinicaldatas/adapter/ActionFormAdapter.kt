package net.clinicaldatas.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import net.clinicaldatas.R
import net.clinicaldatas.utils.DataSingleton
import kotlinx.android.synthetic.main.row_action_form.view.*
import kotlinx.android.synthetic.main.row_vehicle_form.view.*
import net.clinicaldatas.database.DatabaseUtils
import net.clinicaldatas.models.VehicleFormItem


/**
 * Created by doba on 1/21/18.
 */

class ActionFormAdapter(private val context: Context, private val formsDatas: ArrayList<VehicleFormItem>,
                        var onItemClick: (Int) -> Unit) :
        RecyclerView.Adapter<ActionFormAdapter.ViewHolder>() {

    private var TAG: String = "ActionFormAdapter";

    private var currentFormIndex = DataSingleton.instance.currentFormIndex
    private var currentSubId = DataSingleton.instance.currentSubId

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_action_form, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(formsDatas[position], position)
        holder.setIsRecyclable(false)
    }

    override fun getItemCount() = formsDatas.size

    inner class ViewHolder(view: View)
        : RecyclerView.ViewHolder(view) {

        fun bindData(formItem: VehicleFormItem, position: Int) {
            with(formItem) {

                itemView.tv_action_name.text = formItem.vehicleName

                itemView.edt_hai.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(0).id))
                itemView.edt_ba.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(1).id))
                itemView.edt_tu.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(2).id))
                itemView.edt_nam.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(3).id))
                itemView.edt_sau.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(4).id))
                itemView.edt_bay.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(5).id))
                itemView.edt_cn.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(6).id))

                itemView.edt_hai.addTextChangedListener(object : TextWatcher{
                    override fun afterTextChanged(s: Editable?) {
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        if(s.toString().isNotEmpty()){
                            handleInput(position, 0, questionList.get(0).id, s.toString(), eventId, itemView.edt_hai)
                        }
                    }
                })

                itemView.edt_ba.addTextChangedListener(object : TextWatcher{
                    override fun afterTextChanged(s: Editable?) {
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        if(s.toString().isNotEmpty()){
                            handleInput(position, 1, questionList.get(1).id, s.toString(), eventId, itemView.edt_ba)
                        }
                    }
                })

                itemView.edt_tu.addTextChangedListener(object : TextWatcher{
                    override fun afterTextChanged(s: Editable?) {
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        if(s.toString().isNotEmpty()){
                            handleInput(position, 2, questionList.get(2).id, s.toString(), eventId, itemView.edt_tu)
                        }
                    }
                })

                itemView.edt_nam.addTextChangedListener(object : TextWatcher{
                    override fun afterTextChanged(s: Editable?) {
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        if(s.toString().isNotEmpty()){
                            handleInput(position, 3, questionList.get(3).id, s.toString(), eventId, itemView.edt_nam)
                        }
                    }
                })

                itemView.edt_sau.addTextChangedListener(object : TextWatcher{
                    override fun afterTextChanged(s: Editable?) {
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        if(s.toString().isNotEmpty()){
                            handleInput(position, 4, questionList.get(4).id, s.toString(), eventId, itemView.edt_sau)
                        }
                    }
                })

                itemView.edt_bay.addTextChangedListener(object : TextWatcher{
                    override fun afterTextChanged(s: Editable?) {
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        if(s.toString().isNotEmpty()){
                            handleInput(position, 5, questionList.get(5).id, s.toString(), eventId, itemView.edt_bay)
                        }
                    }
                })

                itemView.edt_cn.addTextChangedListener(object : TextWatcher{
                    override fun afterTextChanged(s: Editable?) {
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        if(s.toString().isNotEmpty()){
                            handleInput(position, 6, questionList.get(6).id, s.toString(), eventId, itemView.edt_cn)
                        }
                    }
                })
            }
        }
    }

    private fun handleInput(row: Int, column: Int, questionId: String, value: String, eventId: String, editText: EditText){
        var step = value.toInt()

        when(row){
            0, 1->{//xem TV - xem phim Video/VCD
                if(column in 0..4){
                    var quantity = 300
                    if(step in 0..quantity){
                        DatabaseUtils.addQuestionToDb(currentSubId, questionId, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, value)
                    }else{
                        editText.setText("$quantity")
                        Toast.makeText(context, "Input value must be in [0-$quantity]", Toast.LENGTH_SHORT).show()
                    }
                }else{
                    var quantity = 360
                    if(step in 0..quantity){
                        DatabaseUtils.addQuestionToDb(currentSubId, questionId, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, value)
                    }else{
                        editText.setText("$quantity")
                        Toast.makeText(context, "Input value must be in [0-$quantity]", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            2->{//Su dung may vi tinh de choi
                if(column in 0..6){
                    var quantity = 600
                    if(step in 0..quantity){
                        DatabaseUtils.addQuestionToDb(currentSubId, questionId, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, value)
                    }else{
                        editText.setText("$quantity")
                        Toast.makeText(context, "Input value must be in [0-$quantity]", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            3, 4->{// Su dung may vi tinh de lam bai/Hoc bai lam bai o nha
                if(column in 0..6){
                    var quantity = 480
                    if(step in 0..quantity){
                        DatabaseUtils.addQuestionToDb(currentSubId, questionId, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, value)
                    }else{
                        editText.setText("$quantity")
                        Toast.makeText(context, "Input value must be in [0-$quantity]", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            5->{//Hoc them
                if(column in 0..4){
                    var quantity = 360
                    if(step in 0..quantity){
                        DatabaseUtils.addQuestionToDb(currentSubId, questionId, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, value)
                    }else{
                        editText.setText("$quantity")
                        Toast.makeText(context, "Input value must be in [0-$quantity]", Toast.LENGTH_SHORT).show()
                    }
                }else{
                    var quantity = 480
                    if(step in 0..quantity){
                        DatabaseUtils.addQuestionToDb(currentSubId, questionId, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, value)
                    }else{
                        editText.setText("$quantity")
                        Toast.makeText(context, "Input value must be in [0-$quantity]", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            6->{//Doc sach
                if(column in 0..4){
                    var quantity = 180
                    if(step in 0..quantity){
                        DatabaseUtils.addQuestionToDb(currentSubId, questionId, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, value)
                    }else{
                        editText.setText("$quantity")
                        Toast.makeText(context, "Input value must be in [0-$quantity]", Toast.LENGTH_SHORT).show()
                    }
                }else{
                    var quantity = 300
                    if(step in 0..quantity){
                        DatabaseUtils.addQuestionToDb(currentSubId, questionId, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, value)
                    }else{
                        editText.setText("$quantity")
                        Toast.makeText(context, "Input value must be in [0-$quantity]", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            7->{//Di chuyen di lai
                if(column in 0..6){
                    var quantity = 200
                    if(step in 0..quantity){
                        DatabaseUtils.addQuestionToDb(currentSubId, questionId, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, value)
                    }else{
                        editText.setText("$quantity")
                        Toast.makeText(context, "Input value must be in [0-$quantity]", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            8->{//Lam nhung viec theo y thich
                if(column in 0..6){
                    var quantity = 300
                    if(step in 0..quantity){
                        DatabaseUtils.addQuestionToDb(currentSubId, questionId, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, value)
                    }else{
                        editText.setText("$quantity")
                        Toast.makeText(context, "Input value must be in [0-$quantity]", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            9->{//Ngoi noi chuyen choi
                if(column in 0..6){
                    var quantity = 200
                    if(step in 0..quantity){
                        DatabaseUtils.addQuestionToDb(currentSubId, questionId, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, value)
                    }else{
                        editText.setText("$quantity")
                        Toast.makeText(context, "Input value must be in [0-$quantity]", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            10->{//Choi nhac, ve
                if(column in 0..4){
                    var quantity = 120
                    if(step in 0..quantity){
                        DatabaseUtils.addQuestionToDb(currentSubId, questionId, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, value)
                    }else{
                        editText.setText("$quantity")
                        Toast.makeText(context, "Input value must be in [0-$quantity]", Toast.LENGTH_SHORT).show()
                    }
                }else{
                    var quantity = 200
                    if(step in 0..quantity){
                        DatabaseUtils.addQuestionToDb(currentSubId, questionId, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, value)
                    }else{
                        editText.setText("$quantity")
                        Toast.makeText(context, "Input value must be in [0-$quantity]", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }
}

