package net.clinicaldatas.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import net.clinicaldatas.R
import net.clinicaldatas.database.SubItem
import net.clinicaldatas.models.EventItem
import kotlinx.android.synthetic.main.fragment_question.view.*
import kotlinx.android.synthetic.main.row_sub.view.*

/**
 * Created by doba on 1/21/18.
 */

class SubItemAdapter(private var context : Context, private val eventDatas: List<SubItem>
                     , var onItemClick: (Int) -> Unit) :
        RecyclerView.Adapter<SubItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_sub, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(eventDatas[position], position)

    }

    override fun getItemCount() = eventDatas.size

    inner class ViewHolder(view: View)
        : RecyclerView.ViewHolder(view) {

        fun bindView(eventItem: SubItem, position: Int) {
            with(eventItem) {

                itemView.tv_sub_name.text = eventItem.subEnrollName +" - "+eventItem.subEnrollId;
                itemView.tv_sub_date.text = eventItem.date
                itemView.setOnClickListener {
                    onItemClick(position);
                }
            }
        }
    }
}