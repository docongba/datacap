package net.clinicaldatas.adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import net.clinicaldatas.R
import net.clinicaldatas.models.OrganizeItem

/**
 * Created by doba on 1/20/18.
 */
class HospitalAdapter(private var activity: Activity, private var items: List<OrganizeItem>, var onItemClick: (Int) -> Unit): BaseAdapter() {

    private class ViewHolder(row: View?) {
        var txtName: TextView? = null

        init {
            this.txtName = row?.findViewById<TextView>(R.id.tv_hospital_name)
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.row_hospital, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        var userDto = items[position]
        viewHolder.txtName?.text = userDto.name

        view!!.setOnClickListener{
            onItemClick(position);
        }

        return view as View
    }

    override fun getItem(i: Int): OrganizeItem {
        return items[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getCount(): Int {
        return items.size
    }
}