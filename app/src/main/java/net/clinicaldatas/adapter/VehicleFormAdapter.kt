package net.clinicaldatas.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import net.clinicaldatas.R
import net.clinicaldatas.utils.DataSingleton
import android.widget.CompoundButton
import android.widget.Toast
import kotlinx.android.synthetic.main.row_vehicle_form.view.*
import net.clinicaldatas.database.DatabaseUtils
import net.clinicaldatas.models.VehicleFormItem
import net.clinicaldatas.utils.DataConstant


/**
 * Created by doba on 1/21/18.
 */

class VehicleFormAdapter(private val context : Context, private val formsDatas: ArrayList<VehicleFormItem>,
                         var onItemClick: (Int) -> Unit) :
        RecyclerView.Adapter<VehicleFormAdapter.ViewHolder>() {

    private var TAG : String = "VehicleFormAdapter";
    private var type = 0;

    private var currentFormIndex = DataSingleton.instance.currentFormIndex
    private var currentSubId = DataSingleton.instance.currentSubId

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_vehicle_form, parent, false)

        return ViewHolder(view)
    }

    fun setType(type : Int){
        this.type = type;
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(formsDatas[position], position)
        holder.setIsRecyclable(false)
    }

    override fun getItemCount() = formsDatas.size

    inner class ViewHolder(view: View)
        : RecyclerView.ViewHolder(view) {

        fun bindData(formItem: VehicleFormItem, position: Int) {
            with(formItem) {

                itemView.tv_vehicle_name.text = formItem.vehicleName

                if(DatabaseUtils.getCheckedAnswerQuestion(currentSubId, questionList.get(0).id)){
                    itemView.ck_hai.isChecked = true
                }

                if(DatabaseUtils.getCheckedAnswerQuestion(currentSubId, questionList.get(1).id)){
                    itemView.ck_ba.isChecked = true
                }

                if(DatabaseUtils.getCheckedAnswerQuestion(currentSubId, questionList.get(2).id)){
                    itemView.ck_tu.isChecked = true
                }

                if(DatabaseUtils.getCheckedAnswerQuestion(currentSubId, questionList.get(3).id)){
                    itemView.ck_nam.isChecked = true
                }

                if(DatabaseUtils.getCheckedAnswerQuestion(currentSubId, questionList.get(4).id)){
                    itemView.ck_sau.isChecked = true
                }

                if(type == DataConstant.FORM_VEHICLE){
                    itemView.rl_bay.visibility = View.GONE
                    itemView.rl_cn.visibility = View.GONE

                    if(type == DataConstant.FORM_VEHICLE){
                        itemView.edt_vehicle_tb.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(5).id))
                    }

                }else{//FORM_ACTION
                    itemView.rl_bay.visibility = View.VISIBLE
                    itemView.rl_cn.visibility = View.VISIBLE

                    if(DatabaseUtils.getCheckedAnswerQuestion(currentSubId, questionList.get(5).id)){
                        itemView.ck_bay.isChecked = true
                    }

                    if(DatabaseUtils.getCheckedAnswerQuestion(currentSubId, questionList.get(6).id)){
                        itemView.ck_cn.isChecked = true
                    }

                    itemView.rl_tb.visibility = View.GONE
                }

                itemView.ck_hai.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
                    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                        DatabaseUtils.addQuestionToDb(currentSubId, questionList.get(0).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, isChecked)
                    }
                })

                itemView.ck_ba.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
                    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                        DatabaseUtils.addQuestionToDb(currentSubId, questionList.get(1).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, isChecked)
                    }
                })

                itemView.ck_tu.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
                    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                        DatabaseUtils.addQuestionToDb(currentSubId, questionList.get(2).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, isChecked)
                    }
                })

                itemView.ck_nam.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
                    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                        DatabaseUtils.addQuestionToDb(currentSubId, questionList.get(3).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, isChecked)
                    }
                })

                itemView.ck_sau.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
                    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                        DatabaseUtils.addQuestionToDb(currentSubId, questionList.get(4).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, isChecked)
                    }
                })

                itemView.ck_bay.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
                    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                        DatabaseUtils.addQuestionToDb(currentSubId, questionList.get(5).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, isChecked)
                    }
                })

                itemView.ck_cn.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
                    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                        DatabaseUtils.addQuestionToDb(currentSubId, questionList.get(6).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, isChecked)
                    }
                })

                itemView.edt_vehicle_tb.addTextChangedListener(object :TextWatcher{
                    override fun afterTextChanged(s: Editable?) {
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                        if(s.toString().isNotEmpty()){
                            var step = s.toString().toInt()

                            when(position){
                                0, 3, 4, 5 ->{
                                    if(step in 0..120){
                                        if(type == DataConstant.FORM_VEHICLE){
                                            DatabaseUtils.addQuestionToDb(currentSubId, questionList.get(5).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, s.toString())
                                        }else{
                                            DatabaseUtils.addQuestionToDb(currentSubId, questionList.get(7).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, s.toString())
                                        }
                                    }else{
                                        itemView.edt_vehicle_tb.setText("120")
                                        Toast.makeText(context, "Input value must be in [0-120]", Toast.LENGTH_SHORT).show()
                                    }
                                }
                                1, 2 ->{
                                    if(step in 0..60){
                                        if(type == DataConstant.FORM_VEHICLE){
                                            DatabaseUtils.addQuestionToDb(currentSubId, questionList.get(5).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, s.toString())
                                        }else{
                                            DatabaseUtils.addQuestionToDb(currentSubId, questionList.get(7).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, s.toString())
                                        }
                                    }else{
                                        itemView.edt_vehicle_tb.setText("60")
                                        Toast.makeText(context, "Input value must be in [0-60]", Toast.LENGTH_SHORT).show()
                                    }
                                }
                            }

//                            if(type == DataConstant.FORM_VEHICLE){
//                                DatabaseUtils.addQuestionToDb(currentSubId, questionList.get(5).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, s.toString())
//                            }else{
//                                DatabaseUtils.addQuestionToDb(currentSubId, questionList.get(7).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, s.toString())
//                            }
                        }
                    }
                })
            }
        }
    }

    private fun storeDb(id1: String, id2: String, eventId: String, value: String){
        if(type == DataConstant.FORM_VEHICLE){
            DatabaseUtils.addQuestionToDb(currentSubId, id1, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, value)
        }else{
            DatabaseUtils.addQuestionToDb(currentSubId, id2, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, value)
        }
    }
}

