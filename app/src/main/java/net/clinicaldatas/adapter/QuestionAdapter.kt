package net.clinicaldatas.adapter

import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.squareup.picasso.Picasso
import net.clinicaldatas.R
import net.clinicaldatas.models.AnswerItem
import net.clinicaldatas.models.QuestionItem
import net.clinicaldatas.database.DataValueDbO
import net.clinicaldatas.database.DatabaseUtils
import net.clinicaldatas.database.EventDbO
import net.clinicaldatas.utils.DataSingleton
import net.clinicaldatas.utils.HelpUtils
import io.realm.Realm
import kotlinx.android.synthetic.main.layout_question_item.view.*
import net.clinicaldatas.utils.DataConstant
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by doba on 1/21/18.
 */

class QuestionAdapter(private val context : Context, private val cateIndex: Int, private val questionList: ArrayList<QuestionItem>,
                      var onItemClick: (Int) -> Unit) :
        RecyclerView.Adapter<QuestionAdapter.ViewHolder>() {

    var TAG : String = "QuestionAdapter";

    var realm = Realm.getDefaultInstance()
    var cal = Calendar.getInstance()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_question_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(questionList[position], position)
    }

    private var isFull : Boolean = false

    fun reloadAll(isFull : Boolean){
        this.isFull = isFull
        notifyDataSetChanged()
    }

    override fun getItemCount() : Int{
        if(isFull){
            return questionList.size
        }else{
            return 1
        }
    }

    var answerValue : String = "" ;
    private lateinit var textDate: TextView;

    private var markPos : Int = -1

    fun reloadToShowRedMark(position: Int){
        markPos = position
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View)
        : RecyclerView.ViewHolder(view) {

        fun bindData(questionItem: QuestionItem, position: Int) {
            with(questionItem) {

                itemView.tv_question_index.text = context.getString(R.string.label_question, position + cateIndex + 1);
                itemView.tv_question_description.text = questionItem.displayFormName;

                answerValue = DatabaseUtils.getAnswerQuestion(DataSingleton.instance.currentSubId, questionItem.id)

                val recyclerView = itemView.rc_question;
                recyclerView.layoutManager = LinearLayoutManager(context)

                itemView.img_question_cover.visibility = View.GONE;
                itemView.rl_date.visibility = View.GONE;
                recyclerView.visibility = View.GONE;
                itemView.edt_answer.visibility = View.GONE;
                itemView.edt_answer_number.visibility = View.GONE;

                if(markPos == position){
                    itemView.tv_question_description.text = questionItem.displayFormName+"*";
                    itemView.tv_question_description.setTextColor(Color.RED)
                }else{
                    itemView.tv_question_description.text = questionItem.displayFormName;
                    itemView.tv_question_description.setTextColor(Color.BLACK)
                }

                var answerDatas = questionItem.optionSet.options;

                Log.e(TAG, "CHECK NOW "+questionItem.optionSetValue)

                if (questionItem.optionSetValue) {

                    recyclerView.visibility = View.VISIBLE;

                    if(!TextUtils.isEmpty(questionItem.url)){
                        itemView.img_question_cover.visibility = View.VISIBLE;
                        Picasso.with(context).load(questionItem.url).placeholder(R.color.blue_grey_300).into(itemView.img_question_cover);
                    }else{
                        itemView.img_question_cover.visibility = View.GONE;
                    }

                    for (i in 0..(answerDatas.size - 1)) {
                        answerDatas.get(i).scaleValue = HelpUtils.getCharList().get(i);

                        //Load exist answer if have
                        answerDatas.get(i).check = false
                        if(answerDatas.get(i).code.equals(answerValue)){
                            answerDatas.get(i).check = true
                        }
                    }

                    val adapter = AnswerAdapter(context, answerDatas) {

                        for (i in 0..(answerDatas.size - 1)) {
                            answerDatas.get(i).check = false;
                            if (answerDatas.get(i).code.equals(it.code)) {
                                answerDatas.get(i).check = true;
                                answerValue = answerDatas.get(i).code
                            }
                        }

                        if(position != 0 && it.code.equals(DataConstant.ANSWER_CODE_NO)){

                        }else{
                            if(!it.code.equals(DataConstant.ANSWER_CODE_NO)){
                                onItemClick(position)
                            }else{
                                onItemClick(-1)
                            }
                        }

                        DatabaseUtils.addQuestionToDb(DataSingleton.instance.currentSubId, questionItem.id, DataSingleton.instance.evetList.get(DataSingleton.instance.currentEventIndex).id, DataSingleton.instance.selectedFormList.get(DataSingleton.instance.currentFormIndex).id, answerValue)
                        recyclerView.adapter.notifyDataSetChanged();
                    }

                    recyclerView.adapter = adapter;
                } else {
                    recyclerView.visibility = View.GONE;

                    if (questionItem.valueType.equals("DATE")) {
                        itemView.rl_date.visibility = View.VISIBLE;

                        textDate = itemView.findViewById<TextView>(R.id.tv_date);
                        //Load exist answer if have
                        if(answerValue.isNotEmpty()){
                            textDate.setText(answerValue)
                        }

                        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
                            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                                   dayOfMonth: Int) {
                                cal.set(Calendar.YEAR, year)
                                cal.set(Calendar.MONTH, monthOfYear)
                                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                                val myFormat = HelpUtils.DATE_FORMAT // mention the format you need
                                val sdf = SimpleDateFormat(myFormat, Locale.US)
                                textDate!!.text = sdf.format(cal.getTime())
                                answerValue = sdf.format(cal.getTime())
                            }
                        }

                        itemView.rl_date.setOnClickListener{
                            DatePickerDialog(context,
                                    dateSetListener,
                                    // set DatePickerDialog to point to today's date when it loads up
                                    cal.get(Calendar.YEAR),
                                    cal.get(Calendar.MONTH),
                                    cal.get(Calendar.DAY_OF_MONTH)).show()
                        }

                    } else if (questionItem.valueType.equals("NUMBER")) {

                        //Load exist answer if have
                        if(answerValue.isNotEmpty()){
                            itemView.edt_answer_number.setText(answerValue)
                        }

                        itemView.edt_answer_number.visibility = View.VISIBLE;
                        itemView.edt_answer_number.addTextChangedListener(object : TextWatcher {
                            override fun afterTextChanged(p0: Editable?) {
                            }

                            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                            }

                            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                                answerValue = p0.toString()
                            }
                        })
                    }else if(questionItem.valueType.equals("TEXT")){

                        //Load exist answer if have
                        if(answerValue.isNotEmpty()){
                            itemView.edt_answer.setText(answerValue)
                        }

                        itemView.edt_answer.visibility = View.VISIBLE;
                        itemView.edt_answer.addTextChangedListener(object : TextWatcher {
                            override fun afterTextChanged(p0: Editable?) {
                            }

                            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                            }

                            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                                answerValue = p0.toString()
                            }
                        })
                    }
                }
            }
        }
    }
}