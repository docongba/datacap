package net.clinicaldatas.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import net.clinicaldatas.R
import net.clinicaldatas.models.EventItem
import kotlinx.android.synthetic.main.row_event.view.*
import kotlinx.android.synthetic.main.row_form.view.*
import android.widget.TextView
import android.app.LauncherActivity.ListItem
import android.text.method.ScrollingMovementMethod
import android.util.Log


/**
 * Created by doba on 1/21/18.
 */

class EventAdapter(private var context: Context, private val headerText: String, private val eventDatas: List<EventItem>
                   , var onItemClick: (Int) -> Unit) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TAG = "EventAdapter"

    private val TYPE_HEADER = 0
    private val TYPE_ITEM = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        var view: View
        if (viewType == TYPE_HEADER) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.header_event_list, parent, false)
            return VHHeader(view)
        } else {//if(viewType == TYPE_ITEM) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.row_event, parent, false)
            return ViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == 0) {
            if (holder is VHHeader) {
                holder.txtTitle.setText(headerText)
            }
        } else {
            if (holder is ViewHolder) {
                holder.bindView(getItem(position - 1), position)
            }
        }
    }

    private fun getItem(position: Int): EventItem {
        return eventDatas.get(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (isPositionHeader(position)) TYPE_HEADER else TYPE_ITEM
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    override fun getItemCount() = eventDatas.size + 1

    inner class VHHeader(itemView: View)
        : RecyclerView.ViewHolder(itemView) {
        var txtTitle: TextView

        init {
            txtTitle = itemView.findViewById<TextView>(R.id.tv_study_desc)
            txtTitle.setMovementMethod(ScrollingMovementMethod());
        }
    }

    inner class ViewHolder(view: View)
        : RecyclerView.ViewHolder(view) {

        fun bindView(eventItem: EventItem, position: Int) {
            with(eventItem) {

                if (eventItem.selected) {
                    itemView.tv_event_name.setBackgroundResource(R.drawable.bg_event_selected);
                    itemView.tv_event_name.setTextColor(context.resources.getColor(R.color.white))
                } else {
                    itemView.tv_event_name.setBackgroundResource(R.drawable.bg_event);
                    itemView.tv_event_name.setTextColor(context.resources.getColor(R.color.light_gray))
                }

                itemView.tv_event_name.text = eventItem.name;
                itemView.setOnClickListener {
                    onItemClick(position);
                }
            }
        }
    }
}