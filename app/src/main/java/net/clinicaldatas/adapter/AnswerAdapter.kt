package net.clinicaldatas.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import net.clinicaldatas.R
import net.clinicaldatas.models.AnswerItem
import kotlinx.android.synthetic.main.row_answer.view.*

/**
 * Created by doba on 1/21/18.
 */

class AnswerAdapter(private var context : Context, private val answerDatas: List<AnswerItem>,
                    private val itemClick: (AnswerItem) -> Unit) :
        RecyclerView.Adapter<AnswerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_answer, parent, false)
        return ViewHolder(view, itemClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindAnswerItem(answerDatas[position], position)
    }

    override fun getItemCount() = answerDatas.size

    inner class ViewHolder(view: View, private val itemClick: (AnswerItem) -> Unit)
        : RecyclerView.ViewHolder(view) {

        fun bindAnswerItem(answerItem: AnswerItem, position: Int) {
            with(answerItem) {
                itemView.tv_label_answer.text = answerItem.scaleValue+". "+answerItem.name;
                itemView.setOnClickListener { itemClick(this) }
                itemView.rd_answer.setOnClickListener{itemClick(this)};

                itemView.rd_answer.isChecked = answerItem.check
                if(answerItem.check){
                    itemView.setBackgroundResource(R.drawable.border_event)
                }else{
                    if(position%2 == 0){
                        itemView.setBackgroundColor(context.resources.getColor(R.color.row_chan))
                    }else{
                        itemView.setBackgroundColor(context.resources.getColor(R.color.row_le))
                    }
                }
            }
        }
    }
}