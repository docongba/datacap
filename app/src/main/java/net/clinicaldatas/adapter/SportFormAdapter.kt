package net.clinicaldatas.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import net.clinicaldatas.R
import kotlinx.android.synthetic.main.row_sport_form.view.*
import net.clinicaldatas.models.SportFormItem
import net.clinicaldatas.database.DatabaseUtils
import net.clinicaldatas.models.AnswerItem
import net.clinicaldatas.utils.DataSingleton


/**
 * Created by doba on 1/21/18.
 */

class SportFormAdapter(private val context : Context, private var sportFormsDatas: ArrayList<SportFormItem>,
                       var onItemClick: (Int) -> Unit) :
        RecyclerView.Adapter<SportFormAdapter.ViewHolder>() {

    private var TAG : String = "SportFormAdapter";

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_sport_form, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(holder, sportFormsDatas.get(position), position)
        holder.itemView.setTag(sportFormsDatas.get(position));
        holder.setIsRecyclable(false)
    }

    private var currentFormIndex = DataSingleton.instance.currentFormIndex
    override fun getItemCount() = sportFormsDatas.size

    inner class ViewHolder(view: View)
        : RecyclerView.ViewHolder(view) {

        fun getSportItems(answerDatas: ArrayList<AnswerItem>) : Array<String>{
            var sports = Array<String>(answerDatas.size){""};

            for(i in 0..answerDatas.size - 1){
                sports[i] = answerDatas.get(i).name
            }

            return sports
        }

        fun loadDataFromDb(holder: ViewHolder, sportFormItem: SportFormItem){

            holder.itemView.edt_quest_answer_2.setText(sportFormItem.secondChoice)
            holder.itemView.edt_quest_answer_3.setText(sportFormItem.thirdChoice)

            if(sportFormItem.questionList.size > 3){
                holder.itemView.edt_quest_answer_4.setText(sportFormItem.fourthChoice)
                holder.itemView.edt_quest_answer_5.setText(sportFormItem.fifthChoice)
            }

            var answerDatas = sportFormItem.questionList.get(0).optionSet.options;
            var code =  sportFormItem.firstChoice
            for(i in 0..answerDatas.size - 1){
                if(code.equals(answerDatas.get(i).code)){
                    holder.itemView.spin_sport_1.setSelection(i)
                }
            }
        }

        fun bindData(holder: ViewHolder, sportFormItem: SportFormItem, pos: Int) {
            with(sportFormItem) {

                questionList = sportFormItem.questionList

                var answerDatas = questionList.get(0).optionSet.options;

                val adaptador = ArrayAdapter<String>(context,
                        R.layout.row_sport_item, R.id.tv_sport_name, getSportItems(answerDatas))

                holder.itemView.spin_sport_1.adapter = adaptador
                holder.itemView.spin_sport_1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                        sportFormsDatas.get(pos).firstChoice = answerDatas.get(position).code

                        DatabaseUtils.updateFirstSportForm(sportFormItem.subId, sportFormItem.formId, answerDatas.get(position).code, sportFormItem)
                        DatabaseUtils.addQuestionToDb(sportFormItem.subId, questionList.get(0).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, answerDatas.get(position).code, sportFormId)
                    } // to close the onItemSelected

                    override fun onNothingSelected(parent: AdapterView<*>) {

                    }
                }

                DatabaseUtils.addQuestionToDb(sportFormItem.subId, questionList.get(0).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, answerDatas.get(0).code, sportFormId)

                holder.itemView.edt_quest_answer_2.addTextChangedListener(object :TextWatcher{
                    override fun afterTextChanged(s: Editable?) {

                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                        Log.e(TAG, "onTextChange "+start+" "+before+" "+count+" "+s.toString())

                        if(s.toString().isNotEmpty()){
                            var step = s.toString().toInt()

                            if(step in 0..7){
                                sportFormsDatas.get(pos).secondChoice = s.toString()

                                DatabaseUtils.updateSecondSportForm(sportFormItem.subId, sportFormItem.formId, s.toString(), sportFormItem)
                                DatabaseUtils.addQuestionToDb(sportFormItem.subId, questionList.get(1).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, s.toString(), sportFormId)
                            }else{
                                holder.itemView.edt_quest_answer_2.setText("7")
                                Toast.makeText(context, "Your input must be in [0-7]", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                })

                holder.itemView.edt_quest_answer_3.addTextChangedListener(object :TextWatcher{
                    override fun afterTextChanged(s: Editable?) {

                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                        if(s.toString().isNotEmpty()){
                            var step = s.toString().toInt()
                            if(step in 0..360){
                                sportFormsDatas.get(pos).thirdChoice = s.toString()

                                DatabaseUtils.updateThirdSportForm(sportFormItem.subId, sportFormItem.formId, s.toString(), sportFormItem)
                                DatabaseUtils.addQuestionToDb(sportFormItem.subId, questionList.get(2).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, s.toString(), sportFormId)
                            }else{
                                holder.itemView.edt_quest_answer_3.setText("360")
                                Toast.makeText(context, "Your input must be in [0-360]", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                })

                holder.itemView.tv_quest_title_2.text = questionList.get(1).displayFormName

                if(questionList.size > 3){

                    holder.itemView.tv_quest_title_4.text = questionList.get(3).displayFormName

                    holder.itemView.edt_quest_answer_4.addTextChangedListener(object :TextWatcher{
                        override fun afterTextChanged(s: Editable?) {

                        }

                        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

                        }

                        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                            if(s.toString().isNotEmpty()){
                                var step = s.toString().toInt()

                                if(step in 0..7){
                                    sportFormsDatas.get(pos).fourthChoice = s.toString()

                                    DatabaseUtils.updateFourthSportForm(sportFormItem.subId, sportFormItem.formId, s.toString(), sportFormItem)
                                    DatabaseUtils.addQuestionToDb(sportFormItem.subId, questionList.get(3).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, s.toString(), sportFormId)
                                }else{
                                    holder.itemView.edt_quest_answer_4.setText("7")
                                    Toast.makeText(context, "Your input must be in [0-7]", Toast.LENGTH_SHORT).show()
                                }
                            }
                        }
                    })

                    holder.itemView.edt_quest_answer_5.addTextChangedListener(object :TextWatcher{
                        override fun afterTextChanged(s: Editable?) {

                        }

                        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

                        }

                        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                            if(s.toString().isNotEmpty()){
                                var step = s.toString().toInt()

                                if(step in 0..360){
                                    sportFormsDatas.get(pos).fifthChoice = s.toString()

                                    DatabaseUtils.updateFifthSportForm(sportFormItem.subId, sportFormItem.formId, s.toString(), sportFormItem)
                                    DatabaseUtils.addQuestionToDb(sportFormItem.subId, questionList.get(4).id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, s.toString(), sportFormId)
                                }else{
                                    holder.itemView.edt_quest_answer_5.setText("360")
                                    Toast.makeText(context, "Your input must be in [0-360]", Toast.LENGTH_SHORT).show()
                                }
                            }
                        }
                    })
                }else{
                    holder.itemView.rl_below.visibility = View.GONE
                }

                if(sportFormItem.sportFormId.isNotEmpty()){
                    loadDataFromDb(holder, sportFormItem)
                }
            }
        }
    }
}

