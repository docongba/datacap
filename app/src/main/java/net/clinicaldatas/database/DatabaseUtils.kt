package net.clinicaldatas.database

import android.util.Log
import net.clinicaldatas.models.FormItem
import net.clinicaldatas.models.QuestionItem
import net.clinicaldatas.utils.DataSingleton
import net.clinicaldatas.utils.HelpUtils
import io.realm.Realm
import io.realm.RealmResults
import net.clinicaldatas.models.SportFormItem
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
//import sun.misc.MessageUtils.where
//import org.omg.PortableServer.IdAssignmentPolicyValue.USER_ID





/**
 * Created by doba on 2/22/18.
 */
class DatabaseUtils{

    companion object {
        private val TAG = "DatabaseUtils"

//        fun getAllQuestionsByForm(formId : String, subId : String) : RealmResults<DataValueDbO>{
//            var realm = Realm.getDefaultInstance()
//            return realm.where(DataValueDbO::class.java).equalTo("formId", formId).equalTo("subId", subId).findAll()
//        }

        fun getEvents(subId: String) : RealmResults<EventDbO>{

            var realm = Realm.getDefaultInstance()
            return realm.where(EventDbO::class.java).equalTo("subId", subId).findAll()
        }

        fun getAllSubs() : RealmResults<SubItem>{

            var realm = Realm.getDefaultInstance()
            return realm.where(SubItem::class.java).findAll()
        }

        fun removeSubFromDatabase(subId: String){
            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(SubItem::class.java).equalTo("id", subId).findFirst()
            realm.beginTransaction()

            if(checkExistEvent != null){
                checkExistEvent.deleteFromRealm()
            }

            realm.commitTransaction()
        }

        fun addSubData(subId: String, subName : String){
            var cal = Calendar.getInstance()
            val myFormat = "yyyy-MM-dd" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.US)

            var dataValue: SubItem? = null

            var realm : Realm = Realm.getDefaultInstance()
            realm.beginTransaction()

            // Either update the edited object or create a new one.
            var data = dataValue?: realm.createObject(SubItem::class.java)
            data.subEnrollId = subId
            data.subEnrollName = subName
            data.date = sdf.format(cal.getTime())
            data.id = subId+"_"+subName
            realm.commitTransaction()
        }

        fun removeQuestionToDb(subId: String, questionItemId: String){
            var realm = Realm.getDefaultInstance()
            var  results: RealmResults<DataValueDbO> = realm.where(DataValueDbO::class.java).equalTo("questionId", questionItemId).equalTo("subId", subId).findAll()
            realm.executeTransaction {
                results.deleteAllFromRealm()
            }
        }

        fun addQuestionToDb(subId: String, questionItemId: String, eventId : String, formId : String, answerValue : String){
            var dataValue: DataValueDbO? = null

            var realm = Realm.getDefaultInstance()
            val checkExistQuestion = realm.where(DataValueDbO::class.java).equalTo("questionId", questionItemId).equalTo("subId", subId).findFirst()
            realm.beginTransaction()

            if(checkExistQuestion == null){

                var data = dataValue?: realm.createObject(DataValueDbO::class.java)
                data.questionId = questionItemId
                data.eventId = eventId
                data.subId = subId;
                data.formId = formId
                data.value = answerValue
                data.rowNum = ""
                data.isChecked = false

            }else{
                checkExistQuestion.value = answerValue
            }

            realm.commitTransaction()
        }

        fun addQuestionToDb(subId: String, questionItemId: String, eventId : String, formId : String, answerValue : String, rowNum : String){
            var dataValue: DataValueDbO? = null

            var realm = Realm.getDefaultInstance()
            val checkExistQuestion = realm.where(DataValueDbO::class.java)
                    .equalTo("questionId", questionItemId)
                    .equalTo("subId", subId)
                    .equalTo("rowNum", rowNum)
                    .findFirst()
            realm.beginTransaction()

            if(checkExistQuestion == null){

                var data = dataValue?: realm.createObject(DataValueDbO::class.java)
                data.questionId = questionItemId
                data.eventId = eventId
                data.subId = subId;
                data.formId = formId
                data.value = answerValue
                data.rowNum = rowNum
                data.isChecked = false
            }else{
                checkExistQuestion.value = answerValue
                checkExistQuestion.rowNum = rowNum
                checkExistQuestion.eventId = eventId
            }

            realm.commitTransaction()
        }

        fun addQuestionToDb(subId: String, questionItemId: String, eventId : String, formId : String, answerValue : Boolean){
            var dataValue: DataValueDbO? = null

            var realm = Realm.getDefaultInstance()
            val checkExistQuestion = realm.where(DataValueDbO::class.java).equalTo("questionId", questionItemId).equalTo("subId", subId).findFirst()
            realm.beginTransaction()

            if(checkExistQuestion == null){

                var data = dataValue?: realm.createObject(DataValueDbO::class.java)
                data.questionId = questionItemId
                data.eventId = eventId
                data.subId = subId;
                data.formId = formId
                data.isChecked = answerValue
                data.value = ""
                data.rowNum = ""
            }else{
                checkExistQuestion.isChecked = answerValue
            }

            realm.commitTransaction()
        }

        fun getAnswerQuestion(subId: String, questionId : String) : String{
            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(DataValueDbO::class.java).equalTo("questionId", questionId).equalTo("subId", subId).findFirst()
            realm.beginTransaction()

            var answer = ""

            if(checkExistEvent != null){
                answer = checkExistEvent.value
            }

            realm.commitTransaction()

            return answer
        }

        fun getCheckedAnswerQuestion(subId: String, questionId : String) : Boolean{
            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(DataValueDbO::class.java).equalTo("questionId", questionId).equalTo("subId", subId).findFirst()
            realm.beginTransaction()

            var answer = false

            if(checkExistEvent != null){
                answer = checkExistEvent.isChecked
            }

            realm.commitTransaction()

            return answer
        }


        fun addNewEvent(subId: String, eventId: String){
            var eventValue: EventDbO? = null

            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(EventDbO::class.java).equalTo("programStage", eventId).equalTo("subId", subId).findFirst()
            realm.beginTransaction()

            if(checkExistEvent == null){

                var data = eventValue?: realm.createObject(EventDbO::class.java)
                data.subId = subId
                data.dueDate = HelpUtils.getCurrentDate()
                data.eventDate = HelpUtils.getCurrentDate()
                data.programStage = eventId;
                data.status = "ACTIVE"
            }

            realm.commitTransaction()
        }

        fun updateFormIfCompleted(subId: String, form: FormItem){

            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(FormItemDbO::class.java).equalTo("formId", form.id).equalTo("subId", subId).findFirst()
            realm.beginTransaction()

            if(checkExistEvent != null){
                checkExistEvent.completed = true
            }

            realm.commitTransaction()
        }

        fun addFormToDb(subId: String, form: FormItem, count : Int){
            var eventValue: FormItemDbO? = null

            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(FormItemDbO::class.java).equalTo("formId", form.id).equalTo("subId", subId).findFirst()
            realm.beginTransaction()

            if(checkExistEvent == null){
                var data = eventValue?: realm.createObject(FormItemDbO::class.java)
                data.compulsoryCount = count
                data.formId = form.id
                data.displayName = form.displayName
                data.subId = subId

            }

            realm.commitTransaction()
        }

        fun getCompulsoryCount(subId: String, formId: String) : Int{

            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(FormItemDbO::class.java).equalTo("formId", formId).equalTo("subId", subId).findFirst()
            realm.beginTransaction()

            var count = 0;

            if(checkExistEvent != null){
                count = checkExistEvent.compulsoryCount
            }

            realm.commitTransaction()

            return count
        }

        fun getStepCompulsoryCount(subId: String, formId: String) : Int{

            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(FormItemDbO::class.java).equalTo("formId", formId).equalTo("subId", subId).findFirst()
            realm.beginTransaction()

            var count = 0;

            if(checkExistEvent != null){
                count = checkExistEvent.stepCompulsoryCount
            }

            realm.commitTransaction()

            return count
        }

        fun getLatestQuestionFormIndex(subId: String, formId: String) : Int{

            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(FormItemDbO::class.java).equalTo("formId", formId).equalTo("subId", subId).findFirst()
            realm.beginTransaction()

            var count = 0;

            if(checkExistEvent != null){
                count = checkExistEvent.latestQuestionFormIndex
            }

            realm.commitTransaction()

            return count
        }

        fun updateLatestQuestionFormIndex(subId: String, formId: String, index : Int){

            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(FormItemDbO::class.java).equalTo("formId", formId).equalTo("subId", subId).findFirst()
            realm.beginTransaction()

            if(checkExistEvent != null){
                checkExistEvent.latestQuestionFormIndex = index
            }

            realm.commitTransaction()
        }

        fun checkFormIsCompleted(subId: String, formId: String) : Boolean{
            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(FormItemDbO::class.java).equalTo("formId", formId).equalTo("subId", subId).findFirst()
            realm.beginTransaction()

            var isCompleted = false;

            if(checkExistEvent != null){
                isCompleted = checkExistEvent.completed
            }

            realm.commitTransaction()

            return isCompleted
        }

        fun updateStepCountForm(form: FormItem, stepCount : Int, subId: String){

            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(FormItemDbO::class.java).equalTo("formId", form.id).equalTo("subId", subId).findFirst()
            realm.beginTransaction()

            if(checkExistEvent != null){
                checkExistEvent.stepCompulsoryCount = stepCount
            }

            realm.commitTransaction()
        }

        fun getAllQuestionsByForm(formId : String, subId : String) : RealmResults<DataValueDbO> {

            var realm = Realm.getDefaultInstance()
            return realm.where(DataValueDbO::class.java).equalTo("formId", formId).equalTo("subId", subId).findAll()
        }

        fun getAllQuestionsBySubId(subId : String) : ArrayList<DataValueDbO> {

            var realm = Realm.getDefaultInstance()
            var datas = realm.where(DataValueDbO::class.java).equalTo("subId", subId).findAll()

            var results = realm.copyFromRealm(datas)
            return results as ArrayList<DataValueDbO>
        }

        fun getAllSportGroupForm(formId : String, subId : String) : ArrayList<SportFormItem> {

            var realm = Realm.getDefaultInstance()
            var datas =  realm.where(SportFormItem::class.java).equalTo("formId", formId).equalTo("subId", subId).findAll()

            var results = realm.copyFromRealm(datas)
            return results as ArrayList<SportFormItem>
        }

        fun updateGroupForm(subId: String, formId: String, sportForm : SportFormItem){

            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(SportFormItem::class.java).equalTo("formId", formId)
                    .equalTo("subId", subId)
                    .equalTo("sportFormId", sportForm.sportFormId)
                    .findFirst()
            realm.beginTransaction()

            if(checkExistEvent != null){
                checkExistEvent.firstChoice = sportForm.firstChoice
                checkExistEvent.secondChoice = sportForm.secondChoice
                checkExistEvent.thirdChoice = sportForm.thirdChoice
                checkExistEvent.fourthChoice = sportForm.fourthChoice
                checkExistEvent.fifthChoice = sportForm.fifthChoice
            }

            realm.commitTransaction()
        }

        fun updateFirstSportForm(subId: String, formId: String, firstValue : String, sportForm : SportFormItem){

            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(SportFormItem::class.java).equalTo("formId", formId)
                    .equalTo("subId", subId)
                    .equalTo("sportFormId", sportForm.sportFormId)
                    .findFirst()
            realm.beginTransaction()

            if(checkExistEvent != null){
                checkExistEvent.firstChoice = firstValue
            }

            realm.commitTransaction()
        }

        fun updateSecondSportForm(subId: String, formId: String, secondValue : String, sportForm : SportFormItem){

            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(SportFormItem::class.java).equalTo("formId", formId)
                    .equalTo("subId", subId)
                    .equalTo("sportFormId", sportForm.sportFormId)
                    .findFirst()
            realm.beginTransaction()

            if(checkExistEvent != null){
                checkExistEvent.secondChoice = secondValue
            }

            realm.commitTransaction()
        }

        fun updateThirdSportForm(subId: String, formId: String, thirdValue : String, sportForm : SportFormItem){

            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(SportFormItem::class.java).equalTo("formId", formId)
                    .equalTo("subId", subId)
                    .equalTo("sportFormId", sportForm.sportFormId)
                    .findFirst()
            realm.beginTransaction()

            if(checkExistEvent != null){
                checkExistEvent.thirdChoice = thirdValue
            }

            realm.commitTransaction()
        }

        fun updateFourthSportForm(subId: String, formId: String, fourth : String, sportForm : SportFormItem){

            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(SportFormItem::class.java).equalTo("formId", formId)
                    .equalTo("subId", subId)
                    .equalTo("sportFormId", sportForm.sportFormId)
                    .findFirst()
            realm.beginTransaction()

            if(checkExistEvent != null){
                checkExistEvent.fourthChoice = fourth
            }

            realm.commitTransaction()
        }

        fun updateFifthSportForm(subId: String, formId: String, fifth : String, sportForm : SportFormItem){

            var realm = Realm.getDefaultInstance()

            val checkExistEvent = realm.where(SportFormItem::class.java).equalTo("formId", formId)
                    .equalTo("subId", subId)
                    .equalTo("sportFormId", sportForm.sportFormId)
                    .findFirst()
            realm.beginTransaction()

            if(checkExistEvent != null){
                checkExistEvent.fifthChoice = fifth
            }

            realm.commitTransaction()
        }


        fun addSportFormToDb(subId: String, eventId : String, formId : String, sportFormItem: SportFormItem){
            var dataValue: SportFormItem? = null

            var realm = Realm.getDefaultInstance()
            val checkExistQuestion = realm.where(SportFormItem::class.java)
                    .equalTo("subId", subId)
                    .equalTo("eventId", eventId)
                    .equalTo("sportFormId", sportFormItem.sportFormId)
                    .equalTo("formId", formId)
                    .findFirst()
            realm.beginTransaction()

            if(checkExistQuestion == null){

                var data = dataValue?: realm.createObject(SportFormItem::class.java)
                data.formId = formId
                data.eventId = eventId
                data.subId = subId;
                data.sportFormId = sportFormItem.sportFormId
                data.firstChoice = sportFormItem.firstChoice
                data.secondChoice = sportFormItem.secondChoice
                data.thirdChoice = sportFormItem.thirdChoice
                data.fourthChoice = sportFormItem.fourthChoice
                data.fifthChoice = sportFormItem.fifthChoice
            }

            realm.commitTransaction()
        }

        fun deleteDatabase(){
            Realm.getDefaultInstance().deleteAll()
        }
    }
}