package net.clinicaldatas.database

import net.clinicaldatas.models.FormItem
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by doba on 2/5/18.
 */
open class Student(
        @PrimaryKey open var _ID: Int = 0,
        open var stuSex: Boolean = false,
        open var firstName: String = "",
        open var lastName: String = "",
        open var stuClass: Int = 0
//        open var form : FormItem
)
    : RealmObject() {

    fun copy(
            _ID: Int = this._ID,
            stuSex: Boolean = this.stuSex,
            firstName: String = this.firstName,
            lastName: String = this.lastName,
            stuClass: Int = this.stuClass)
            = Student(_ID, stuSex, firstName, lastName, stuClass)
}