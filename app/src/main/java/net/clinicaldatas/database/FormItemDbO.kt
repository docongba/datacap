package net.clinicaldatas.database

import net.clinicaldatas.models.ProgramStageSubSection
import net.clinicaldatas.models.QuestionItem
import io.realm.RealmObject

/**
 * Created by doba on 1/21/18.
 */

open class FormItemDbO : RealmObject(){
    var subId : String = ""
    var displayName : String = "";
    var formId : String = "";
    var selected : Boolean = false;
    var completed : Boolean = false
    var compulsoryCount : Int = 0
    var stepCompulsoryCount : Int  = 0

    var latestQuestionFormIndex : Int = 0
}