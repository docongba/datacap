package net.clinicaldatas.database

import io.realm.RealmObject

/**
 * Created by doba on 2/5/18.
 */
open class SubItem : RealmObject(){
    var id : String = ""
    var subEnrollId : String = ""
    var subEnrollName : String = ""

    var date : String = ""
//    var inputEnrollId : String = ""
//    var inputEnrollName : String = ""
}