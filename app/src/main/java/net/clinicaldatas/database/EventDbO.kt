package net.clinicaldatas.database

import io.realm.RealmObject
import io.realm.annotations.Ignore

/**
 * Created by doba on 2/5/18.
 */
open class EventDbO : RealmObject(){

    /**event id*/
    var subId : String = ""
    var programStage : String = "";
    var dueDate  : String = "";
    var status  : String = "";
    var eventDate : String = "";
}