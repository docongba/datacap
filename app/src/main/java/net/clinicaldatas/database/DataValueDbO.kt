package net.clinicaldatas.database

import io.realm.RealmObject

/**
 * Created by doba on 2/5/18.
 */
open class DataValueDbO : RealmObject(){

    /* id of Question*/
    var questionId: String = "";
    var value : String = ""

    var eventId : String = ""
    var formId : String = ""
    var subId : String = ""
    var compulsory : Boolean = false
    var isSynced : Boolean = false

    //Use for checkbox question
    var isChecked : Boolean = false

    //It is the same value of sportFormId
    var rowNum : String = ""

}