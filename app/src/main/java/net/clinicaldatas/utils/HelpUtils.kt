package net.clinicaldatas.utils

import android.content.Context
import net.clinicaldatas.models.AnswerItem
import android.content.res.TypedArray
import android.graphics.Color
import java.text.SimpleDateFormat
import java.util.*
import android.net.NetworkInfo
import android.net.ConnectivityManager




/**
 * Created by doba on 1/22/18.
 */
class HelpUtils{
    companion object {

        const val DATE_FORMAT = "yyyy-MM-dd"
        const val COM_ID = "kZSm9XfYlOu"
        const val WEEK_DAY_ID = "cSrjc8DGvfQ"
        const val WORKING_DAY_ID = "RESXmv3ZZ9I"
        const val FOMR_5 = "qk3ntyIFIyP"

        fun getCharList() : ArrayList<String>{
            var chars = ArrayList<String>();

            chars.add("1")
            chars.add("2")
            chars.add("3")
            chars.add("4")
            chars.add("5")
            chars.add("6")
            chars.add("7")
            chars.add("8")
            chars.add("9")
            chars.add("10")
            chars.add("11")
            chars.add("12")
            chars.add("13")
            chars.add("14")
            chars.add("15")
            chars.add("16")
            chars.add("17")
            chars.add("18")
            chars.add("19")
            chars.add("20")
            chars.add("21")
            chars.add("22")
            chars.add("23")
            chars.add("24")
            chars.add("25")
            chars.add("26")
            chars.add("27")
            chars.add("28")
            chars.add("29")
            chars.add("30")
            chars.add("31")
            chars.add("32")
            chars.add("33")
            chars.add("34")
            chars.add("35")
            chars.add("36")
            chars.add("37")
            chars.add("38")
            chars.add("39")
            chars.add("40")



            return chars;
        }

        fun getRiceIdList() : ArrayList<String>{
            var chars = ArrayList<String>();

            chars.add("1")
            chars.add("2")
            chars.add("3")
            chars.add("4")
            chars.add("5")
            chars.add("6")
            chars.add("7")
            chars.add("8")
            chars.add("9")
            chars.add("10")
            chars.add("11")
            chars.add("12")
            chars.add("13")
            chars.add("14")
            chars.add("15")
            chars.add("16")

            return chars;
        }

        fun getMatColor(context: Context, typeColor: String): Int {
            var returnColor = Color.BLACK
            val arrayId = context.getResources().getIdentifier("mdcolor_" + typeColor, "array", context.getApplicationContext().getPackageName())

            if (arrayId != 0) {
                val colors = context.getResources().obtainTypedArray(arrayId)
                val index = (Math.random() * colors.length()).toInt()
                returnColor = colors.getColor(index, Color.BLACK)
                colors.recycle()
            }
            return returnColor
        }

        fun getDateValue(cal: Calendar, year : Int, monthOfYear: Int, dayOfMonth : Int) : String{
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val myFormat = DATE_FORMAT // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            return sdf.format(cal.getTime())
        }

        fun getCurrentDate() : String{

            var cal = Calendar.getInstance()
            var date = cal.time
            val myFormat = DATE_FORMAT // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.US)

            return sdf.format(date)
        }

        fun isOnline(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.activeNetworkInfo
            return netInfo != null && netInfo.isConnectedOrConnecting
        }
    }



//    companion object {
//        fun
//    }
}