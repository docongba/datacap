package net.clinicaldatas.utils

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

/**
 * Created by doba on 2/5/18.
 */
enum class MessageEventBus {

    INSTANCE;

    private val bus = PublishSubject.create<String>() // the actual publisher handling all of the events

    fun send(message: String) {
        bus.onNext(message) // the message being sent to all subscribers
    }

    fun toObserverable(): Observable<String> {
        return bus // return the publisher itself as an observable to subscribe to
    }

}