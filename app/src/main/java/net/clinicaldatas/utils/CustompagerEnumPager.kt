package net.clinicaldatas.utils

import net.clinicaldatas.R

/**
 * Created by doba on 2/1/18.
 */
enum class CustomPagerEnum private constructor(val titleResId: Int, val layoutResId: Int) {

    RED(R.string.label_new, R.layout.fragment_fill_student),
    BLUE(R.string.label_existing, R.layout.fragment_exist),

}