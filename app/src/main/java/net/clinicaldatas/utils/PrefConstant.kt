package net.clinicaldatas.utils

/**
 * Created by doba on 2/3/18.
 */
class PrefConstant{
    companion object {
        var PREF_LATEST_ID = "latest_id";
        var PREF_USERNAME = "username";
        var PREF_PASS = "pass"
        var PREF_LOGIN = "is_logined"
    }
}