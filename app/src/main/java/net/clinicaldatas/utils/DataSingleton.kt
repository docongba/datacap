package net.clinicaldatas.utils

import net.clinicaldatas.models.EventItem
import net.clinicaldatas.models.FormItem
import net.clinicaldatas.models.OrganizeItem
import net.clinicaldatas.models.ProgramItem

/**
 * Created by doba on 1/23/18.
 */
public class DataSingleton private constructor() {
    init {
        println("This ($this) is a singleton")
    }

    private object Holder {
        val INSTANCE = DataSingleton()
    }

    companion object {
        val instance: DataSingleton by lazy { Holder.INSTANCE }
    }

    var studyList: ArrayList<ProgramItem> = ArrayList<ProgramItem>();
    var evetList: ArrayList<EventItem> = ArrayList<EventItem>();
    var formList: ArrayList<FormItem> = ArrayList<FormItem>();
    var selectedFormList = ArrayList<FormItem>()
    var organizations: ArrayList<OrganizeItem> = ArrayList<OrganizeItem>();
    var trackedEntity: String = "";
    lateinit var currentStudyId: String;
    var trackedEntityAttributes: ArrayList<ProgramItem.ProgramTrackedEntity> = ArrayList<ProgramItem.ProgramTrackedEntity>();

    var currentEventIndex: Int = 0;
    var currentFormIndex: Int = 0;
    var programId: String = "";
    var username: String = "";
    var password: String = "";
    var orgUnit: String = ""
    var currentSubId: String = ""

    fun reset() {
        studyList.clear()
        evetList.clear()
        formList.clear()
        selectedFormList.clear()
        organizations.clear()
        trackedEntityAttributes.clear()

        currentFormIndex = 0;
        currentEventIndex = 0
    }
}
