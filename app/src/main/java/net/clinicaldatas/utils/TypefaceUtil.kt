package net.clinicaldatas.utils

import android.content.Context
import java.lang.reflect.AccessibleObject.setAccessible
import android.graphics.Typeface
import android.util.Log


/**
 * Created by doba on 1/25/18.
 */
class TypefaceUtil{

    companion object {
        fun overrideFont(context: Context, defaultFontNameToOverride: String, customFontFileNameInAssets: String) {
            try {
                val customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets)

                val defaultFontTypefaceField = Typeface::class.java.getDeclaredField(defaultFontNameToOverride)
                defaultFontTypefaceField.isAccessible = true
                defaultFontTypefaceField.set(null, customFontTypeface)
            } catch (e: Exception) {
                Log.e("nemo", "can not set font");
            }

        }
    }

}