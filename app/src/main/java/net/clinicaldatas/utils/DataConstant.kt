package net.clinicaldatas.utils

/**
 * Created by doba on 1/28/18.
 */
class DataConstant{

    companion object {
        var PREF_LATEST_FORM : String = "latest_form";
        var PREF_LATEST_EVENT : String = "latest_event";
        var FORM_VEHICLE : Int = 100
        var FORM_ACTION : Int = 101

        var KEY_FORM_TYPE : String = "form_type"
        var KEY_FORM_WEEKDAY : String = "form_weekday"

        var ANSWER_CODE_NO = "0"
    }
}