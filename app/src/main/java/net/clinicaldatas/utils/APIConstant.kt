package net.clinicaldatas.utils

/**
 * Created by doba on 1/30/18.
 */
class APIConstant{
    companion object {

        const val TEST_USERNAME = "android";
        const val TEST_PASS = "Horus@123";

        const val BASE_API : String = "https://clinicaldatas.net/phamngocthach/";
        const val LOGIN : String = "api/26/me.json?fields=*,attributeValues[*],organisationUnits[*],authorities[*],userCredentials[*,userRoles[*,programs[*,programTrackedEntityAttributes[*,trackedEntityAttribute[*]]]]]";
        const val METADATA_PRE : String = "api/26/programs.json?fields=id,name,shortName,displayDescription,version,displayName,description,relatedProgram[id,displayName],trackedEntity[id,displayName],organisationUnits[id,displayName],userRoles[id,displayName],programStages[id,name,displayName,description,sortOrder,programStageDataElements[dataElement,compulsory,renderOptionsAsRadio,renderOptionsAsRadioMultiSelect,allowFutureDate],programStageSections[programStageSubSections[id,name,dataElements[url,id,name,formName,displayFormName,valueType,optionSetValue,optionSet[code,name,options[code,name,sortOrder]]]],id,displayName,groupForm,sortOrder,dataElements[url,id,name,formName,displayFormName,valueType,optionSetValue,optionSet[code,name,options[code,name,sortOrder]]]]],programTrackedEntityAttributes[id,name,displayName,mandatory,valueType,optionSetValue,allowFutureDate,sortOrder,trackedEntityAttribute[id,unique]]&paging=false&filter=id:in:[";
        const val METADATA_POST : String = "]";

        const val SYNC_DATA: String = "api/27/mobileevents/mobileData";
    }
}