package net.clinicaldatas.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.layout_question_list.*
import kotlinx.android.synthetic.main.layout_question_list.view.*
import net.clinicaldatas.R
import net.clinicaldatas.`interface`.OnboardingCateFragmentDelegate
import net.clinicaldatas.adapter.QuestionAdapter
import net.clinicaldatas.database.DatabaseUtils
import net.clinicaldatas.models.FormItem
import net.clinicaldatas.models.QuestionItem
import net.clinicaldatas.utils.DataSingleton
import net.clinicaldatas.utils.DataConstant
import kotlin.collections.ArrayList

/**
 * Created by doba on 2/22/18.
 */
class QuestionCategoryFragment : Fragment() {

    private var TAG: String = "QuestionCategoryFragment";

    lateinit var formCheck : FormItem;
    var questionList = ArrayList<QuestionItem>();

    lateinit var questionAdapter : QuestionAdapter;
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.layout_question_list, container, false)

        var index = arguments.getInt(ARG_SUB_INDEX)

        formCheck = DataSingleton.instance.selectedFormList.get(DataSingleton.instance.currentFormIndex);
        questionList = formCheck.programStageSubSections.get(index).dataElements

        var cateIndex = 0

        if(index != 0){
            for(i in 1..index){
                cateIndex = cateIndex + questionList.size
            }
        }

        var programStageDataElements = DataSingleton.instance.evetList.get(DataSingleton.instance.currentEventIndex).programStageDataElements
        for(i in 0 until questionList.size){

            for(j in 0 until programStageDataElements.size){
                if(questionList.get(i).id.equals(programStageDataElements.get(j).dataElement.id) && programStageDataElements.get(j).compulsory){
                    questionList.get(i).compulsory = true
                }
            }
        }

        questionAdapter = QuestionAdapter(activity , cateIndex, questionList){
            isFull = it >= 0
            questionAdapter.reloadAll(isFull)

            if(isFull){
                rootView.rcQuestist.layoutManager.smoothScrollToPosition(rootView.rcQuestist, null, questionList.size - 1)
            }else{
                for(i in 0 until questionList.size){
                    DatabaseUtils.removeQuestionToDb(DataSingleton.instance.currentSubId, questionList[i].id)
                }
            }
        }

        var firstQuestion  = questionList.get(0)
        if(DatabaseUtils.getAnswerQuestion(DataSingleton.instance.currentSubId, firstQuestion.id).isNotEmpty()
            && !DatabaseUtils.getAnswerQuestion(DataSingleton.instance.currentSubId, firstQuestion.id).equals(DataConstant.ANSWER_CODE_NO)){
            isFull = true
        }

        if(isFull){
            questionAdapter.reloadAll(isFull)
        }

        rootView.rcQuestist.layoutManager = LinearLayoutManager(activity)
        rootView.rcQuestist.adapter = questionAdapter

        rootView.imgNext.setOnClickListener{
            if(allowMove(questionList)){
                delegate?.onQuestionCateFragmentNext(this)
            }
        }

        rootView.imgBack.setOnClickListener{
            delegate?.onQuestionCateFragmentBack(this)
        }

        if(index == 0){
            rootView.imgBack.visibility = View.GONE
        }

        return rootView
    }

    private var isFull : Boolean = false

    private fun allowMove(questionList: ArrayList<QuestionItem>) : Boolean{

        if(isFull){
            for(i in 0 until questionList.size){
                if(DatabaseUtils.getAnswerQuestion(DataSingleton.instance.currentSubId, questionList.get(i).id).isEmpty()){
                    questionAdapter.reloadToShowRedMark(i)
                    Toast.makeText(activity, "Please answer the question: "+questionList.get(i).displayFormName, Toast.LENGTH_SHORT).show()
                    return false
                }
            }
        }else{
            for(i in 0 until questionList.size){
                if(DatabaseUtils.getAnswerQuestion(DataSingleton.instance.currentSubId, questionList.get(i).id).isEmpty()
                        && questionList.get(i).compulsory){
                    questionAdapter.reloadToShowRedMark(i)
                    Toast.makeText(activity, "Please answer the question: "+questionList.get(i).displayFormName, Toast.LENGTH_SHORT).show()
                    return false
                }
            }
        }

        return true;
    }

    var delegate: OnboardingCateFragmentDelegate? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is OnboardingCateFragmentDelegate) {
            delegate = context
        }
    }

    companion object {
        private val ARG_SUB_INDEX = "section_number"
        private val ARG_EVENT_ID = "event_id"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(sectionNumber: Int, eventId : String): QuestionCategoryFragment {
            val fragment = QuestionCategoryFragment()
            val args = Bundle()
            args.putInt(ARG_SUB_INDEX, sectionNumber)
            args.putString(ARG_EVENT_ID, eventId)
            fragment.arguments = args
            return fragment
        }
    }
}