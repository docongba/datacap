package net.clinicaldatas.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import net.clinicaldatas.R

@SuppressLint("ValidFragment")
/**
 * Created by doba on 1/27/18.
 */
/**
 * A placeholder fragment containing a simple view.
 */

//class FormsFragment : Fragment() {
//    override fun onCreateView(inflater: LayoutInflater?, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
//        return inflater!!.inflate(R.layout.fragment_fill_student, container, false)
//    }
//}

class ExistItemFragment : Fragment() {

    private var TAG: String = "ExistItemFragment";


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_exist, container, false)


        return rootView
    }

    fun initData() {
    }

    override fun onResume() {
        super.onResume()
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private val tabIndext = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(sectionNumber: Int): ExistItemFragment {
            val fragment = ExistItemFragment()
            val args = Bundle()
            args.putInt(tabIndext, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }
}