package net.clinicaldatas.fragment

import android.app.Fragment
import net.clinicaldatas.models.FormItem
import net.clinicaldatas.models.QuestionItem

/**
 * Created by doba on 3/22/18.
 */
open class BaseQuestionFragment : Fragment(){

    private val TAG : String = "BaseQuestionFragment"

    lateinit var formCheck : FormItem;
    var questionList = ArrayList<QuestionItem>();
}