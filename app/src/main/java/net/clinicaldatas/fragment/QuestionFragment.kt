package net.clinicaldatas.fragment

import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.squareup.picasso.Picasso
import net.clinicaldatas.R
import net.clinicaldatas.`interface`.OnboardingFragmentDelegate
import net.clinicaldatas.activity.QuestionActivity
import net.clinicaldatas.adapter.AnswerAdapter
import net.clinicaldatas.customview.RectangleImageView
import net.clinicaldatas.models.AnswerItem
import net.clinicaldatas.models.QuestionItem
import net.clinicaldatas.database.DatabaseUtils
import net.clinicaldatas.utils.DataSingleton
import net.clinicaldatas.utils.HelpUtils
import java.util.*

/**
 * Created by doba on 2/12/18.
 */
class QuestionFragment : Fragment() {

    private var TAG: String = "QuestionFragment";

    private lateinit var questItem: QuestionItem;
    private lateinit var recyclerView: RecyclerView;
    private lateinit var answerDatas: List<AnswerItem>;
    var cal = Calendar.getInstance()
    private lateinit var textDate: TextView;
    private var isChecked : Boolean = false;
    var answerValue : String = "" ;
    var eventId : String = "";
    var currentSubId : String = ""
    var currentFormIndex : Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_question, container, false)

        val quetionIndexView = rootView.findViewById<TextView>(R.id.tv_question_index);
        var questIndex = arguments.getInt(ARG_POSITION)

        currentSubId = DataSingleton.instance.currentSubId
        currentFormIndex = DataSingleton.instance.currentFormIndex

        quetionIndexView.text = getString(R.string.label_question, questIndex);

        questItem = arguments.getSerializable(ARG_QUESTION) as QuestionItem;
        eventId = arguments.getString(ARG_EVENT_ID);

        answerValue = DatabaseUtils.getAnswerQuestion(currentSubId, questItem.id)

        val textDescription = rootView.findViewById<TextView>(R.id.tv_question_description);
        textDescription.text = questItem.displayFormName;

        val edtText = rootView.findViewById<EditText>(R.id.edt_answer);
        val edtNumber = rootView.findViewById<EditText>(R.id.edt_answer_number);

        recyclerView = rootView.findViewById<RecyclerView>(R.id.rc_question);
        recyclerView.layoutManager = LinearLayoutManager(activity)

        val imgCover = rootView.findViewById<RectangleImageView>(R.id.img_question_cover);
        imgCover.visibility = View.GONE;

        val nextBut = rootView.findViewById<ImageView>(R.id.imgNext);
        nextBut.setOnClickListener{
            if(isChecked){

                if(questItem.compulsory){
                    QuestionActivity.stepCompulsoryCount++
                    DatabaseUtils.updateStepCountForm(DataSingleton.instance.selectedFormList.get(currentFormIndex), QuestionActivity.stepCompulsoryCount, currentSubId)
                }

                DatabaseUtils.addQuestionToDb(currentSubId, questItem.id, eventId, DataSingleton.instance.selectedFormList.get(currentFormIndex).id, answerValue)
                delegate?.onQuestionFragmentNext(this)
            }else{
                Toast.makeText(activity, "Please answer the question", Toast.LENGTH_SHORT).show()
                textDescription.setTextColor(Color.RED)
                textDescription.text = questItem.displayFormName+"*"
            }
        }

        val nextBack = rootView.findViewById<ImageView>(R.id.imgBack);
        nextBack.setOnClickListener{
            delegate?.onQuestionFragmentBack(this)
        }

        if(questIndex == 1){
            nextBack.visibility = View.GONE
        }

        val rlDate = rootView.findViewById<RelativeLayout>(R.id.rl_date);
        rlDate.visibility = View.GONE;
        recyclerView.visibility = View.GONE;
        edtText.visibility = View.GONE;
        edtNumber.visibility = View.GONE;

        if (questItem.optionSetValue) {
            recyclerView.visibility = View.VISIBLE;

            if(!TextUtils.isEmpty(questItem.url)){
                imgCover.visibility = View.VISIBLE;
                Picasso.with(context).load(questItem.url).placeholder(R.color.blue_grey_300).into(imgCover);
            }else{
                imgCover.visibility = View.GONE;
            }

            answerDatas = questItem.optionSet.options;
            for (i in 0..(answerDatas.size - 1)) {
                answerDatas.get(i).scaleValue = HelpUtils.getCharList().get(i);

                //Load exist answer if have
                answerDatas.get(i).check = false
                if(answerDatas.get(i).code.equals(answerValue)){
                    answerDatas.get(i).check = true
                    isChecked = true
                }
            }

            val adapter = AnswerAdapter(context, answerDatas) {
                isChecked = true;
                for (i in 0..(answerDatas.size - 1)) {
                    answerDatas.get(i).check = false;
                    if (answerDatas.get(i).code.equals(it.code)) {
                        answerDatas.get(i).check = true;
                        answerValue = answerDatas.get(i).code
                    }
                }

                recyclerView.adapter.notifyDataSetChanged();
            }
            recyclerView.adapter = adapter;
        } else {
            recyclerView.visibility = View.GONE;

            if (questItem.valueType.equals("DATE")) {
                rlDate.visibility = View.VISIBLE;

                textDate = rootView.findViewById<TextView>(R.id.tv_date);

                //Load exist answer if have
                if(answerValue.isNotEmpty()){
                    isChecked = true
                    textDate.setText(answerValue)
                }

                val dateSetListener = object : DatePickerDialog.OnDateSetListener {
                    override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                           dayOfMonth: Int) {

                        isChecked = true;
                        val dateValue = HelpUtils.getDateValue(cal, year, monthOfYear, dayOfMonth)

                        textDate!!.text = dateValue
                        answerValue = dateValue
                    }
                }

                rlDate.setOnClickListener{
                    DatePickerDialog(activity,
                            dateSetListener,
                            // set DatePickerDialog to point to today's date when it loads up
                            cal.get(Calendar.YEAR),
                            cal.get(Calendar.MONTH),
                            cal.get(Calendar.DAY_OF_MONTH)).show()
                }

            } else if (questItem.valueType.equals("NUMBER")) {

                //Load exist answer if have
                if(answerValue.isNotEmpty()){
                    isChecked = true
                    edtNumber.setText(answerValue)
                }

                edtNumber.visibility = View.VISIBLE;
                edtNumber.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) {
                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        isChecked = p0.toString().length > 0

                        answerValue = p0.toString()
                    }
                })
            }else if(questItem.valueType.equals("TEXT")){

                //Load exist answer if have
                if(answerValue.isNotEmpty()){
                    isChecked = true
                    edtText.setText(answerValue)
                }

                edtText.visibility = View.VISIBLE;
                edtText.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) {
                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        isChecked = p0.toString().length > 0

                        answerValue = p0.toString()
                    }
                })
            }
        }

        return rootView
    }

    var delegate: OnboardingFragmentDelegate? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is OnboardingFragmentDelegate) {
            delegate = context
        }
    }

    companion object {
        private val ARG_POSITION = "section_number"
        private val ARG_EVENT_ID = "event_id"
        private val ARG_QUESTION = "question_item";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(sectionNumber: Int, eventId : String, questionItem: QuestionItem): QuestionFragment {
            val fragment = QuestionFragment()
            val args = Bundle()
            args.putInt(ARG_POSITION, sectionNumber)
            args.putString(ARG_EVENT_ID, eventId)
            args.putSerializable(ARG_QUESTION, questionItem);
            fragment.arguments = args
            return fragment
        }
    }
}