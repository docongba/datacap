package net.clinicaldatas.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import net.clinicaldatas.BuildConfig
import net.clinicaldatas.R
import net.clinicaldatas.activity.QuestionActivity
import net.clinicaldatas.adapter.FormAdapter
import net.clinicaldatas.models.FormItem
import net.clinicaldatas.utils.DataSingleton

@SuppressLint("ValidFragment")
/**
 * Created by doba on 1/27/18.
 */
/**
 * A placeholder fragment containing a simple view.
 */

//class FormsFragment : Fragment() {
//    override fun onCreateView(inflater: LayoutInflater?, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
//        return inflater!!.inflate(R.layout.fragment_fill_student, container, false)
//    }
//}

class NewItemFragment : Fragment() {

    private var TAG: String = "NewItemFragment";


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_fill_student, container, false)

        val editSubId = rootView.findViewById<View>(R.id.edt_sub_id) as EditText
        val edtSubName = rootView.findViewById<View>(R.id.edt_sub_name) as EditText;

        if(BuildConfig.DEBUG){
            editSubId.setText("222334");
            edtSubName.setText("Nemo");
        }

//        val butNext = dialogView.findViewById<View>(R.id.but_next) as Button;
//        butNext.setOnClickListener(View.OnClickListener {
//
//            if(edtSubName.text.trim().isEmpty() || editSubId.text.trim().isEmpty()){
//                Toast.makeText(this, "Do not allow empty", Toast.LENGTH_SHORT).show();
//            }else{
//                dialog.dismiss()
//                val intent = Intent(this, EventListActivity::class.java);
//                intent.putExtra("study_name", DataSingleton.instance.studyList.get(0).name);
//                intent.putExtra("study_date", DataSingleton.instance.studyList.get(0).date);
//                intent.putExtra("study_id", DataSingleton.instance.studyList.get(0).id);
//                intent.putExtra("study_description", DataSingleton.instance.studyList.get(0).description);
//                startActivity(intent);
//
//            }
//        })




        return rootView
    }

    fun initData() {
    }

    override fun onResume() {
        super.onResume()
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private val tabIndext = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(sectionNumber: Int): NewItemFragment {
            val fragment = NewItemFragment()
            val args = Bundle()
            args.putInt(tabIndext, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }
}