package net.clinicaldatas.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import net.clinicaldatas.R
import net.clinicaldatas.activity.QuestionActivity
import net.clinicaldatas.activity.RiceActivity
import net.clinicaldatas.adapter.FormAdapter
import net.clinicaldatas.database.DatabaseUtils
import net.clinicaldatas.models.EventItem
import net.clinicaldatas.models.FormItem
import net.clinicaldatas.models.QuestionItem
import net.clinicaldatas.utils.DataSingleton
import net.clinicaldatas.activity.SportFormActivity
import net.clinicaldatas.activity.VehicleFormActivity
import net.clinicaldatas.utils.DataConstant
import net.clinicaldatas.utils.HelpUtils

@SuppressLint("ValidFragment")
/**
 * Created by doba on 1/27/18.
 */
/**
 * A placeholder fragment containing a simple view.
 */

class FormsFragment : Fragment() {

    private var TAG: String = "FormsFragment";

    private lateinit var formsDatas: ArrayList<FormItem>;
    private lateinit var recyclerView: RecyclerView;
    private lateinit var adapter : FormAdapter;
    var currentSubId : String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_forms, container, false)

        recyclerView = rootView.findViewById<RecyclerView>(R.id.rc_event_list);
        recyclerView.layoutManager = LinearLayoutManager(activity)

        currentSubId = DataSingleton.instance.currentSubId

        return rootView
    }

    fun isSpecialForm(formItem: FormItem) : Boolean{

        if(formItem.groupForm){
            return true
        }

        if(formItem.id.equals(HelpUtils.COM_ID)
                || formItem.id.equals(HelpUtils.WORKING_DAY_ID)
                || formItem.id.equals(HelpUtils.WEEK_DAY_ID)){
            return true
        }
        return false
    }

    fun initData(tabIndex : Int) {

        formsDatas = ArrayList<FormItem>()
        var forms = DataSingleton.instance.formList;
        if(tabIndex == 0){
            for(i in 0..forms.size -1){
                if((DatabaseUtils.getAllQuestionsByForm(forms.get(i).id, currentSubId).size == forms.get(i).dataElements.size && !isSpecialForm(forms.get(i)))
                        || DatabaseUtils.checkFormIsCompleted(currentSubId, forms.get(i).id)){

                }else{
                    formsDatas.add(forms.get(i))
                }
            }
        }else{
            for(i in 0..forms.size -1){
                if((DatabaseUtils.getAllQuestionsByForm(forms.get(i).id, currentSubId).size == forms.get(i).dataElements.size && !isSpecialForm(forms.get(i)))
                        || DatabaseUtils.checkFormIsCompleted(currentSubId, forms.get(i).id)){
                    forms.get(i).selected = false
                    formsDatas.add(forms.get(i))
                }
            }
        }

        var programStageDataElements = DataSingleton.instance.evetList.get(DataSingleton.instance.currentEventIndex).programStageDataElements

        var compulsoryCount = 0

        for (i in 0..formsDatas.size - 1){
            for(j in 0..formsDatas.get(i).dataElements.size - 1){
                if(checkCompulsory(programStageDataElements, formsDatas.get(i).dataElements.get(j))){
                    formsDatas.get(i).dataElements.get(j).compulsory = true
                    compulsoryCount++
                }
            }

            DatabaseUtils.addFormToDb(currentSubId, formsDatas.get(i), compulsoryCount)
            compulsoryCount = 0
        }
    }

    fun checkCompulsory(programStageDataElements : ArrayList<EventItem.ProgramStageDataElement>, questionItem: QuestionItem) : Boolean{
        for(i in 0..programStageDataElements.size-1){
            if(questionItem.id.equals(programStageDataElements.get(i).dataElement.id) && programStageDataElements.get(i).compulsory){
                return true
            }
        }

        return false
    }

    override fun onResume() {
        super.onResume()

        var index = arguments.getInt(TAB_INDEX);

        initData(index);

        adapter = FormAdapter(activity, formsDatas) {

            DataSingleton.instance.currentFormIndex = it;

            for(i in 0..formsDatas.size - 1){
                formsDatas.get(i).selected = false;
                if(i == it){
                    formsDatas.get(i).selected = true;
                }
            }

            Log.e(TAG, "formCheck "+formsDatas.get(it).sortOrder+" "+formsDatas.get(it).groupForm)
            DataSingleton.instance.selectedFormList = formsDatas

            val bundle = Bundle()
            var intent :Intent
            if(formsDatas.get(it).id.equals(HelpUtils.COM_ID)){
                intent = Intent(activity, RiceActivity::class.java);
            }else if(formsDatas.get(it).groupForm){
                intent = Intent(activity, SportFormActivity::class.java);
            }else if(formsDatas.get(it).id.equals(HelpUtils.WEEK_DAY_ID)) {
                intent = Intent(activity, VehicleFormActivity::class.java);
                intent.putExtra(DataConstant.KEY_FORM_TYPE, DataConstant.FORM_ACTION)
            }else if(formsDatas.get(it).id.equals(HelpUtils.WORKING_DAY_ID)) {
                intent = Intent(activity, VehicleFormActivity::class.java);
                intent.putExtra(DataConstant.KEY_FORM_TYPE, DataConstant.FORM_VEHICLE)
            }else if(formsDatas.get(it).id.equals(HelpUtils.FOMR_5)){
                intent = Intent(activity, VehicleFormActivity::class.java);
                intent.putExtra(DataConstant.KEY_FORM_TYPE, DataConstant.FORM_VEHICLE)
            }
            else{
                var latestQuestion = DatabaseUtils.getLatestQuestionFormIndex(currentSubId, formsDatas.get(it).id)

                intent = Intent(activity, QuestionActivity::class.java);
                intent.putExtra("position", latestQuestion)
            }

            intent.putExtras(bundle)

            startActivity(intent);
        }

        recyclerView.adapter = adapter;
        if(adapter != null){
            adapter.notifyDataSetChanged();
        }
    }

    companion object {
        private val TAB_INDEX = "section_number"

        fun newInstance(sectionNumber: Int): FormsFragment {
            val fragment = FormsFragment()
            val args = Bundle()
            args.putInt(TAB_INDEX, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }
}