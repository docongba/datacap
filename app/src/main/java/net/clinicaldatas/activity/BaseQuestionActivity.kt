package net.clinicaldatas.activity

import android.view.MenuItem
import android.view.View
import net.clinicaldatas.activity.BaseActivity
import net.clinicaldatas.models.FormItem
import net.clinicaldatas.models.QuestionItem
import net.clinicaldatas.utils.DataSingleton

/**
 * Created by doba on 3/21/18.
 */
abstract class BaseQuestionActivity : BaseActivity() {

    private val TAG :String = "BaseQuestionActivity"

    lateinit var formCheck: FormItem;
    var currentFormIndex = 0
    var currentEventIndex = 0;
    var currentSubId = "";
    var currentEventId = ""
    lateinit var selectedFormList : ArrayList<FormItem>;

    lateinit var questionList: ArrayList<QuestionItem>;

    fun initialBaseData(){
        currentFormIndex = DataSingleton.instance.currentFormIndex
        currentEventIndex = DataSingleton.instance.currentEventIndex;
        currentSubId = DataSingleton.instance.currentSubId
        selectedFormList = DataSingleton.instance.selectedFormList
        formCheck = selectedFormList.get(currentFormIndex);
        currentEventId = DataSingleton.instance.evetList.get(currentEventIndex).id

        questionList = formCheck.dataElements;
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item!!.getItemId()
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}