package net.clinicaldatas.activity

import android.content.Intent
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import net.clinicaldatas.R
import net.clinicaldatas.database.DatabaseUtils
import net.clinicaldatas.models.EventItem
import net.clinicaldatas.models.QuestionItem
import net.clinicaldatas.utils.DataSingleton
import kotlinx.android.synthetic.main.activity_rice.*

/**
 * Created by doba on 1/18/18.
 */
class RiceActivity : BaseQuestionActivity {

    private val TAG = "RiceActivity"

    constructor() : super()

    companion object {
        var stepCompulsoryCount = 0
    }

    override fun getResourceLayout(): Int {
        return R.layout.activity_rice;
    }

    override fun initViews() {
        val toolBar = findViewById<Toolbar>(R.id.tool_bar);
        setSupportActionBar(toolBar);
        supportActionBar!!.setDisplayHomeAsUpEnabled(true);
        supportActionBar!!.setDisplayShowHomeEnabled(true);
        supportActionBar!!.setTitle(R.string.label_com);

        initialBaseData()

        var programStageDataElements = DataSingleton.instance.evetList.get(currentEventIndex).programStageDataElements

        RiceActivity.stepCompulsoryCount = DatabaseUtils.getStepCompulsoryCount(currentSubId, formCheck.id)

        for (i in 0..questionList.size - 1) {
            if (checkCompulsory(programStageDataElements, questionList.get(i))) {
                questionList.get(i).compulsory = true
            }
        }

        edt_sang_lung.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(0).id))
        edt_sang_vua.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(1).id))
        edt_sang_day.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(2).id))
        edt_sang_to.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(3).id))
        edt_trua_lung.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(4).id))
        edt_trua_vua.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(5).id))
        edt_trua_day.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(6).id))
        edt_trua_to.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(7).id))
        edt_chieu_lung.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(8).id))
        edt_chieu_vua.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(9).id))
        edt_chieu_day.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(10).id))
        edt_chieu_to.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(11).id))
        edt_phu_lung.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(12).id))
        edt_phu_vua.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(13).id))
        edt_phu_day.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(14).id))
        edt_phu_to.setText(DatabaseUtils.getAnswerQuestion(currentSubId, questionList.get(15).id))

        processEditText(edt_sang_lung, 0)
        processEditText(edt_trua_lung, 4)
        processEditText(edt_chieu_lung, 8)
        processEditText(edt_phu_lung, 12)
        processEditText(edt_sang_vua, 1)
        processEditText(edt_trua_vua, 5)
        processEditText(edt_chieu_vua, 9)
        processEditText(edt_phu_vua, 13)
        processEditText(edt_sang_day, 2)
        processEditText(edt_trua_day, 6)
        processEditText(edt_chieu_day, 10)
        processEditText(edt_phu_day, 14)
        processEditText(edt_sang_to, 3)
        processEditText(edt_trua_to, 7)
        processEditText(edt_chieu_to, 11)
        processEditText(edt_phu_to, 15)

        val nextBut = findViewById<ImageView>(R.id.imgNext);
        nextBut.setOnClickListener{
            if (QuestionActivity.stepCompulsoryCount == DatabaseUtils.getCompulsoryCount(currentSubId, formCheck.id)
                    || DatabaseUtils.getAllQuestionsByForm(formCheck.id, currentSubId).size == formCheck.dataElements.size) {
                DatabaseUtils.updateFormIfCompleted(currentSubId, formCheck)
            }

            checkField(questionList.get(0), edt_sang_lung)
            checkField(questionList.get(1), edt_sang_vua)
            checkField(questionList.get(2), edt_sang_day)
            checkField(questionList.get(3), edt_sang_to)
            checkField(questionList.get(4), edt_trua_lung)
            checkField(questionList.get(5), edt_trua_vua)
            checkField(questionList.get(6), edt_trua_day)
            checkField(questionList.get(7), edt_trua_to)
            checkField(questionList.get(8), edt_chieu_lung)
            checkField(questionList.get(9), edt_chieu_vua)
            checkField(questionList.get(10), edt_chieu_day)
            checkField(questionList.get(11), edt_chieu_to)
            checkField(questionList.get(12), edt_phu_lung)
            checkField(questionList.get(13), edt_phu_vua)
            checkField(questionList.get(14), edt_phu_day)
            checkField(questionList.get(15), edt_phu_to)

            val intent = Intent(baseContext, CompleteActivity::class.java);
            startActivity(intent)
            finish();
        }
    }

    fun processEditText(editText: EditText, questionNo : Int){
        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                if (questionList.get(questionNo).compulsory) {
                    QuestionActivity.stepCompulsoryCount++
                    DatabaseUtils.updateStepCountForm(selectedFormList.get(currentFormIndex), QuestionActivity.stepCompulsoryCount, currentSubId)

                }
                DatabaseUtils.addQuestionToDb(currentSubId, questionList.get(questionNo).id, DataSingleton.instance.evetList.get(currentEventIndex).id, selectedFormList.get(currentFormIndex).id, p0.toString())
            }
        })
    }

    override fun initData() {
    }

    fun checkField(questionItem: QuestionItem, editText: EditText){
        if(questionItem.compulsory){
            if(editText.text.toString().trim().isEmpty()){
                editText.setError("Fill this field")
                return
            }
        }
    }

    fun checkCompulsory(programStageDataElements: ArrayList<EventItem.ProgramStageDataElement>, questionItem: QuestionItem): Boolean {
        for (i in 0..programStageDataElements.size - 1) {
            if (questionItem.id.equals(programStageDataElements.get(i).dataElement.id) && programStageDataElements.get(i).compulsory) {
                return true
            }
        }

        return false
    }

    override fun onClick(v: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}