package net.clinicaldatas.activity

import android.content.Intent

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import net.clinicaldatas.R
import net.clinicaldatas.`interface`.OnboardingCateFragmentDelegate
import net.clinicaldatas.`interface`.OnboardingFragmentDelegate
import net.clinicaldatas.customview.NonSwipePager
import net.clinicaldatas.database.DatabaseUtils
import net.clinicaldatas.fragment.QuestionCategoryFragment
import net.clinicaldatas.fragment.QuestionFragment
import net.clinicaldatas.models.EventItem
import net.clinicaldatas.models.FormItem
import net.clinicaldatas.models.QuestionItem
import net.clinicaldatas.utils.DataSingleton
import io.realm.Realm

import kotlin.collections.ArrayList

class QuestionActivity : BaseQuestionActivity(), OnboardingFragmentDelegate, OnboardingCateFragmentDelegate {

    private val TAG: String = "QuestionActivity";

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private lateinit var menuItemNo : MenuItem

    companion object {
        var stepCompulsoryCount = 0
    }

    private lateinit var pagerView : ViewPager;

    override fun getResourceLayout(): Int {
        return R.layout.activity_question;
    }

    override fun initViews() {

        val toolBar = findViewById<Toolbar>(R.id.tool_bar);
        setSupportActionBar(toolBar);
        supportActionBar!!.setDisplayHomeAsUpEnabled(true);
        supportActionBar!!.setDisplayShowHomeEnabled(true);

        initialBaseData()

        supportActionBar!!.title = formCheck.displayName;

        var programStageDataElements = DataSingleton.instance.evetList.get(DataSingleton.instance.currentEventIndex).programStageDataElements

        stepCompulsoryCount = DatabaseUtils.getStepCompulsoryCount(currentSubId, formCheck.id)

        for(i in 0..questionList.size - 1){
            if(checkCompulsory(programStageDataElements, questionList.get(i))){
                questionList.get(i).compulsory = true
            }
        }

        loadPagerView()
    }

    override fun initData() {
    }

    fun checkCompulsory(programStageDataElements : ArrayList<EventItem.ProgramStageDataElement>, questionItem: QuestionItem) : Boolean{
        for(i in 0..programStageDataElements.size-1){
            if(questionItem.id.equals(programStageDataElements.get(i).dataElement.id) && programStageDataElements.get(i).compulsory){
                return true
            }
        }

        return false
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_question, menu)

        menuItemNo = menu.findItem(R.id.action_no)

        var noValue = DatabaseUtils.getAllQuestionsByForm(formCheck.id, currentSubId).size.toString() +"/"+questionList.size
        menuItemNo.setTitle(noValue)

        return true
    }

    fun loadPagerView(){

        var container : LinearLayout = findViewById<LinearLayout>(R.id.ll_question_container);
        var v: View = LayoutInflater.from(this).inflate(R.layout.layout_question_pager, null)

        container.addView(v)

        pagerView = v.findViewById<NonSwipePager>(R.id.pager_question)

        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        pagerView.adapter = mSectionsPagerAdapter

        var index = DatabaseUtils.getLatestQuestionFormIndex(currentSubId, formCheck.id)

        pagerView.setCurrentItem(index)
        pagerView.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }
            override fun onPageSelected(position: Int) {

                DatabaseUtils.updateLatestQuestionFormIndex(currentSubId, formCheck.id, position)
                //Finished / Total
                var noValue = DatabaseUtils.getAllQuestionsByForm(formCheck.id, currentSubId).size.toString() +"/"+questionList.size
                menuItemNo.setTitle(noValue)
            }
        })
    }

    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            if(formCheck.programStageSubSections.size > 0){
                return QuestionCategoryFragment.newInstance(position, DataSingleton.instance.evetList.get(DataSingleton.instance.currentEventIndex).id)
            }else{
                return QuestionFragment.newInstance(position + 1, DataSingleton.instance.evetList.get(DataSingleton.instance.currentEventIndex).id, questionList.get(position))
            }
        }

        override fun getCount(): Int {
            if(formCheck.programStageSubSections.size > 0){
                return formCheck.programStageSubSections.size
            }else{
                return questionList.size
            }
        }
    }

    override fun onQuestionFragmentNext(fragment: QuestionFragment) {

        if(QuestionActivity.stepCompulsoryCount == DatabaseUtils.getCompulsoryCount(currentSubId, formCheck.id)
                || DatabaseUtils.getAllQuestionsByForm(formCheck.id, currentSubId).size == formCheck.dataElements.size){
            DatabaseUtils.updateFormIfCompleted(currentSubId, formCheck)
        }

        if (pagerView.currentItem < (mSectionsPagerAdapter!!.count - 1)) {
            pagerView.setCurrentItem(pagerView.currentItem + 1);
        }else{
            val intent = Intent(baseContext, CompleteActivity::class.java);
            startActivity(intent)
            finish();
        }
    }

    override fun onQuestionFragmentBack(fragment: QuestionFragment) {
        if (pagerView.currentItem != 0) {
            pagerView.setCurrentItem(pagerView.currentItem - 1);
        }
    }

    override fun onQuestionCateFragmentNext(fragment: QuestionCategoryFragment) {
        if(QuestionActivity.stepCompulsoryCount == DatabaseUtils.getCompulsoryCount(currentSubId, formCheck.id)
                || DatabaseUtils.getAllQuestionsByForm(formCheck.id, currentSubId).size == questionList.size){
            DatabaseUtils.updateFormIfCompleted(currentSubId, formCheck)
        }

        if (pagerView.currentItem < (mSectionsPagerAdapter!!.count - 1)) {
            pagerView.setCurrentItem(pagerView.currentItem + 1);
        }else{
            val intent = Intent(baseContext, CompleteActivity::class.java);
            startActivity(intent)
            finish();
        }
    }

    override fun onQuestionCateFragmentBack(fragment: QuestionCategoryFragment) {
        if (pagerView.currentItem != 0) {
            pagerView.setCurrentItem(pagerView.currentItem - 1);
        }
    }
}
