package net.clinicaldatas.activity

import android.content.Context
import android.content.Intent
import android.support.design.widget.TabLayout
import android.support.design.widget.TextInputLayout
import android.support.v4.view.ViewPager
import android.widget.*
import net.clinicaldatas.R
import net.clinicaldatas.adapter.HospitalAdapter
import android.view.animation.AnimationUtils
import net.clinicaldatas.models.OrganizeItem
import net.clinicaldatas.retrofit.response.ApiInterface
import net.clinicaldatas.retrofit.response.MetadataResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.support.v4.view.PagerAdapter
import android.support.v7.app.AlertDialog
import android.view.*
import android.view.inputmethod.InputMethodManager
import net.clinicaldatas.BuildConfig
import net.clinicaldatas.database.SubItem
import net.clinicaldatas.database.DatabaseUtils
import net.clinicaldatas.utils.*
import io.realm.Realm
import android.graphics.Typeface
import android.widget.TextView


/**
 * Created by doba on 1/20/18.
 */
class MainActivity : BaseActivity, AdapterView.OnItemSelectedListener {
    constructor() : super()

    private val TAG: String = "MainActivity";

    private lateinit var dialog: AlertDialog;
    private lateinit var spinHospital : Spinner
    private lateinit var listOfItems: List<OrganizeItem>

    override fun getResourceLayout(): Int {
        return R.layout.activity_main;
    }

    override fun initData() {

        loadMetaData()

        listOfItems = DataSingleton.instance.organizations;

        DataSingleton.instance.orgUnit = listOfItems.get(0).id
        val arrayAdapter = HospitalAdapter(this, listOfItems){
            DataSingleton.instance.orgUnit = listOfItems.get(it).id
        };
        spinHospital.adapter = arrayAdapter;
    }

    override fun initViews() {
        spinHospital = findViewById(R.id.spin_hospital);
        spinHospital.setOnItemSelectedListener(this);

        var llEnroll = findViewById<LinearLayout>(R.id.ll_enroll);
        var llSync = findViewById<LinearLayout>(R.id.ll_sync);
        var llProfile = findViewById<LinearLayout>(R.id.ll_profile)
        var llLogout = findViewById<LinearLayout>(R.id.ll_logout);

        llEnroll.setOnClickListener{
            showNewNameDialog()
        }

        llSync.setOnClickListener{
            startActivity(Intent(this, SyncActivity::class.java));
        }

        llLogout.setOnClickListener{
            finish()
            DataSingleton.instance.reset()
            startActivity(Intent(this, LoginActivity::class.java));
        }

        llProfile.setOnClickListener{
            startActivity(Intent(this, ProfileActivity::class.java));
        }

        val imgBanner = findViewById<ImageView>(R.id.img_banner);
        val animation = AnimationUtils.loadAnimation(applicationContext, R.anim.zoom_in_animation)
        imgBanner.startAnimation(animation)
    }

    override fun onResume() {
        super.onResume()

        DataSingleton.instance.currentEventIndex = 0
        DataSingleton.instance.currentFormIndex = 0

        for(i in 0..DataSingleton.instance.formList.size - 1){
            DataSingleton.instance.formList.get(i).selected = false
        }

        for(i in 0..DataSingleton.instance.selectedFormList.size - 1){
            DataSingleton.instance.selectedFormList.get(i).selected = false
        }

        for(i in 0..DataSingleton.instance.evetList.size - 1){
            DataSingleton.instance.evetList.get(i).selected = false
            for(j in 0..DataSingleton.instance.evetList.get(i).programStageSections.size - 1){
                DataSingleton.instance.evetList.get(i).programStageSections.get(j).selected = false
            }
        }
    }

    override fun onClick(p0: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {

    }

    fun showNewNameDialog() {
        val dialogBuilder  = AlertDialog.Builder(this, R.style.myDialog)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_enroll, null)
        dialogBuilder.setView(dialogView)

        val adapter = CustomPagerAdapter(this);
        val viewPager = dialogView.findViewById<ViewPager>(R.id.dialog_pager_container)
        viewPager.adapter = adapter

        val tabLayout = dialogView.findViewById<View>(R.id.dialog_tabs) as TabLayout
        tabLayout.setupWithViewPager(viewPager)

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {

            }
        })

        val titleOnGoing = getString(R.string.label_new);
        val titleCompleted = getString(R.string.label_existing);
        tabLayout.getTabAt(0)!!.setText(titleOnGoing)
        tabLayout.getTabAt(1)!!.setText(titleCompleted)

        var llInputName = dialogView.findViewById<TextInputLayout>(R.id.input_layout_name);
        llInputName.visibility = View.GONE
        var llInputId = dialogView.findViewById<TextInputLayout>(R.id.input_layout_id);
        var editSubId = dialogView.findViewById<View>(R.id.edt_sub_id) as EditText
        editSubId.requestFocus()

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm!!.showSoftInput(editSubId, InputMethodManager.SHOW_IMPLICIT)

        var editSubName = dialogView.findViewById<View>(R.id.edt_sub_name) as EditText;

        llInputId.setHint(DataSingleton.instance.trackedEntityAttributes.get(0).trackedEntityAttribute.name);

        if(BuildConfig.DEBUG){
            editSubId.setText("nemo")
        }

        if(DataSingleton.instance.trackedEntityAttributes.size > 1){
            llInputName.visibility = View.VISIBLE
            llInputName.setHint(DataSingleton.instance.trackedEntityAttributes.get(1).trackedEntityAttribute.name);

            if(BuildConfig.DEBUG){
                editSubName.setText("nemo")
            }
        }

        val butNext = dialogView.findViewById<View>(R.id.but_next) as Button;
        butNext.setOnClickListener{
            if (editSubId.text.toString().isEmpty() || editSubName.text.toString().isEmpty()) {
                Toast.makeText(this, "Do not allow empty", Toast.LENGTH_SHORT).show();
            } else {

                var enrollSubId = editSubId.text.toString().trim()
                var enrollSubName = editSubName.text.toString().trim()

                addSubData(enrollSubId, enrollSubName)

                PreferenceUtils.saveStringPref(baseContext, PrefConstant.PREF_LATEST_ID, editSubId.text.toString());
                dialog.dismiss()
                val intent = Intent(this, EventListActivity::class.java);
                intent.putExtra("study_name", DataSingleton.instance.studyList.get(0).name);
                intent.putExtra("study_date", DataSingleton.instance.studyList.get(0).date);
                intent.putExtra("study_id", DataSingleton.instance.studyList.get(0).id);
                intent.putExtra("study_description", DataSingleton.instance.studyList.get(0).description);
                startActivity(intent);
            }
        }

        val butCancel = dialogView.findViewById<View>(R.id.but_cancel) as Button;
        butCancel.setOnClickListener{
            dialog.dismiss()
        }

        val labelHocSinh = dialogView.findViewById<TextView>(R.id.tv_title)
        val face = Typeface.createFromAsset(assets, "fonts/Roboto-Regular.ttf")
        butCancel.typeface = face
        butNext.typeface = face
        editSubId.typeface = face
        editSubName.typeface = face
        labelHocSinh.typeface = face

        setCustomFont(tabLayout)

        dialog = dialogBuilder.create()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        dialog.show()
    }

    fun setCustomFont(mCustomFontTab : TabLayout) {

        var vg = mCustomFontTab.getChildAt(0) as ViewGroup;
        var tabsCount : Int = vg.getChildCount();

        for (j in 0..tabsCount - 1) {
            var vgTab =  vg.getChildAt(j) as ViewGroup;
            var tabChildsCount  = vgTab.getChildCount();

            for ( i in 0..tabChildsCount - 1) {
                var tabViewChild = vgTab.getChildAt(i) as View;
                if (tabViewChild is TextView) {
                    //Put your font in assests folder
                    //assign name of the font here (Must be case sensitive)
                    tabViewChild.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf"));
                }
            }
        }
    }

    fun addSubData(subId: String, subName : String){

        DataSingleton.instance.currentSubId = subId+"_"+subName

        if(!checkExistSub(DataSingleton.instance.currentSubId)){
            DatabaseUtils.addSubData(subId, subName)
        }
    }

    fun checkExistSub(subId: String) : Boolean{
        var realm : Realm = Realm.getDefaultInstance()
        return realm.where(SubItem::class.java).equalTo("id", subId).findAll().size > 0
    }

    fun loadMetaData() {

        showLoading()
        val apiService = ApiInterface.createService(ApiInterface::class.java, DataSingleton.instance.username, DataSingleton.instance.password)

        val url = APIConstant.METADATA_PRE + DataSingleton.instance.programId + APIConstant.METADATA_POST;
        val call = apiService.getMetadata(url)
        call.enqueue(object : Callback<MetadataResponse> {
            override fun onResponse(call: Call<MetadataResponse>, response: Response<MetadataResponse>?) {
                if (response != null) {

                    var metaData = response.body() as MetadataResponse;
                    DataSingleton.instance.studyList = metaData.programs;
                    DataSingleton.instance.trackedEntity = metaData.programs.get(0).trackedEntity.id
                }

                hideLoading()
            }

            override fun onFailure(call: Call<MetadataResponse>, t: Throwable) {
                hideLoading()
            }
        })
    }

    inner class CustomPagerAdapter(private val mContext: Context) : PagerAdapter() {

        override fun instantiateItem(collection: ViewGroup, position: Int): Any {
            val customPagerEnum = CustomPagerEnum.values()[position]
            val inflater = LayoutInflater.from(mContext)
            val layout = inflater.inflate(customPagerEnum.layoutResId, collection, false) as ViewGroup
            collection.addView(layout)

            return layout
        }

        override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
            collection.removeView(view as View)
        }

        override fun getCount(): Int {
            return CustomPagerEnum.values().size
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object`
        }

        override fun getPageTitle(position: Int): CharSequence {
            val customPagerEnum = CustomPagerEnum.values()[position]
            return mContext.getString(customPagerEnum.titleResId)
        }
    }
}