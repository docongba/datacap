package net.clinicaldatas.activity

import android.content.Intent
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.widget.Button
import android.widget.ImageView
import net.clinicaldatas.R
import net.clinicaldatas.database.DatabaseUtils
import net.clinicaldatas.adapter.SportFormAdapter
import net.clinicaldatas.models.SportFormItem

/**
 * Created by doba on 1/18/18.
 */
class SportFormActivity : BaseQuestionActivity {

    private val TAG = "SportFormActivity"

    constructor() : super()

    private lateinit var sportForms: ArrayList<SportFormItem>;
    private lateinit var sportsRc : RecyclerView;
    private lateinit var sportFormAdapter : SportFormAdapter;

    override fun getResourceLayout(): Int {
        return R.layout.activity_sport;
    }

    override fun initViews() {
        val toolBar = findViewById<Toolbar>(R.id.tool_bar);
        setSupportActionBar(toolBar);
        supportActionBar!!.setDisplayHomeAsUpEnabled(true);
        supportActionBar!!.setDisplayShowHomeEnabled(true);

        initialBaseData()

        supportActionBar!!.setTitle(formCheck.displayName);

        val nextBut = findViewById<ImageView>(R.id.imgNext);
        nextBut.setOnClickListener{
            DatabaseUtils.updateFormIfCompleted(currentSubId, formCheck)

            val intent = Intent(baseContext, CompleteActivity::class.java);
            startActivity(intent)
            finish();
        }

        val addBut = findViewById<Button>(R.id.but_more)
        addBut.setOnClickListener{
            var lastIndex = sportForms.size
            var sportFormItem = createNewSportFormItem(lastIndex.toString())

            DatabaseUtils.addSportFormToDb(currentSubId, currentEventId, formCheck.id, sportFormItem)
            sportForms.clear()
            sportForms.addAll(getAllSportFormInDb())

            sportsRc.adapter = sportFormAdapter

            scrollViewToBottom()
        }

        sportForms = getAllSportFormInDb()

        //Initial if database is empty
        if(sportForms.size == 0){
            var sportFormItem = createNewSportFormItem("0")

            DatabaseUtils.addSportFormToDb(currentSubId, currentEventId, formCheck.id, sportFormItem)
            sportForms.add(sportFormItem)
        }

        sportFormAdapter = SportFormAdapter(baseContext, sportForms){

        }

        sportsRc = findViewById<RecyclerView>(R.id.rc_sport_form);
        sportsRc.layoutManager = LinearLayoutManager(baseContext)
        sportsRc.adapter = sportFormAdapter
    }

    override fun initData() {
    }

    fun scrollViewToBottom(){
        var mHandler = Handler();
        mHandler!!.postDelayed(Runnable {

            sportsRc.layoutManager.smoothScrollToPosition(sportsRc, null, sportForms.size - 1);

        }, 100);
    }

    //create new one
    fun createNewSportFormItem(sportFormId : String) : SportFormItem{
        var sportItem = SportFormItem()
        sportItem.subId = currentSubId
        sportItem.sportFormId = sportFormId
        sportItem.questionList = questionList
        sportItem.eventId = currentEventId
        sportItem.formId = formCheck.id

        return sportItem
    }

    fun getAllSportFormInDb() : ArrayList<SportFormItem>{

        var datas = DatabaseUtils.getAllSportGroupForm(formCheck.id, currentSubId);
        for(i in 0..datas.size-1){
            datas.get(i).questionList = questionList
        }

        return datas;
    }
}