package net.clinicaldatas.activity

import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.*
import net.clinicaldatas.R
import net.clinicaldatas.adapter.VehicleFormAdapter
import net.clinicaldatas.models.*
import net.clinicaldatas.utils.DataConstant
import kotlinx.android.synthetic.main.header_vehicle.*
import net.clinicaldatas.adapter.ActionFormAdapter
import net.clinicaldatas.database.DatabaseUtils


/**
 * Created by doba on 1/18/18.
 */
class VehicleFormActivity : BaseQuestionActivity {

    private val TAG = "VehicleFormActivity"

    constructor() : super()

    override fun getResourceLayout(): Int {
        return R.layout.activity_vehicle;
    }

    override fun initViews() {
        val toolBar = findViewById<Toolbar>(R.id.tool_bar);
        setSupportActionBar(toolBar);
        supportActionBar!!.setDisplayHomeAsUpEnabled(true);
        supportActionBar!!.setDisplayShowHomeEnabled(true);

        initialBaseData()

        supportActionBar!!.setTitle(formCheck.displayName);

        var vehicleForms = ArrayList<VehicleFormItem>()

        var type = intent.getIntExtra(DataConstant.KEY_FORM_TYPE, 0)

        if(type == DataConstant.FORM_VEHICLE){
            tv_header_bay.visibility = View.GONE
            tv_header_cn.visibility = View.GONE
        }else{
            tv_time_tb.visibility = View.GONE
        }

        if(type == DataConstant.FORM_VEHICLE){
            var sportNames = resources.getStringArray(R.array.vehicle_list)
            vehicleForms.addAll(getVehicles(sportNames, 6))
        }else{
            var sportNames = resources.getStringArray(R.array.action_list)
            vehicleForms.addAll(getVehicles(sportNames, 7))
        }

        var sportsRc = findViewById<RecyclerView>(R.id.rc_vehicle_form);
        sportsRc.layoutManager = LinearLayoutManager(baseContext)
        if(type == DataConstant.FORM_VEHICLE){
            var vehicleFormAdapter = VehicleFormAdapter(baseContext, vehicleForms){

            }

            vehicleFormAdapter.setType(type)
            sportsRc.adapter = vehicleFormAdapter
        }else{
            var actionFormAdapter = ActionFormAdapter(baseContext, vehicleForms){

            }

            sportsRc.adapter = actionFormAdapter
        }

        val nextBut = findViewById<ImageView>(R.id.imgNext);
        nextBut.setOnClickListener{
            //Add the last one to DB
            DatabaseUtils.updateFormIfCompleted(currentSubId, formCheck)

            val intent = Intent(baseContext, CompleteActivity::class.java);
            startActivity(intent)
            finish();
        }
    }

    fun getVehicles(sportNames : Array<String>, numQuestInGroup : Int) : ArrayList<VehicleFormItem>{

        var datas = ArrayList<VehicleFormItem>()
        var questStep = 0;

        for(i in 0..sportNames.size - 1){
            var item = VehicleFormItem()
            item.eventId = currentEventId
            item.formId = formCheck.id
            item.subId = currentSubId
            item.vehicleName = sportNames.get(i)
            for(m in 0..numQuestInGroup - 1){
                item.questionList.add(questionList.get(m+questStep))
            }

            questStep = questStep + numQuestInGroup
            datas.add(item)
        }

        return datas
    }

    override fun initData() {
    }
}