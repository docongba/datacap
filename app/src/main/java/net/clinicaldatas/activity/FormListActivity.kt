package net.clinicaldatas.activity

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.DatePicker
import android.widget.RelativeLayout
import android.widget.TextView
import net.clinicaldatas.R
import net.clinicaldatas.database.DatabaseUtils
import net.clinicaldatas.fragment.FormsFragment
import net.clinicaldatas.models.EventItem
import net.clinicaldatas.models.FormItem
import net.clinicaldatas.utils.DataSingleton
import net.clinicaldatas.utils.HelpUtils
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by doba on 1/21/18.
 */
class FormListActivity : BaseActivity {

    private var TAG : String = "FormListActivity";

    constructor() : super()
    private lateinit var formsDatas: List<FormItem>;
    private var onGoingSize : Int = 0;
    private lateinit var tvDate : TextView;
    private lateinit var rlEventDate : RelativeLayout;
    private lateinit var tabLayout : TabLayout
    var cal = Calendar.getInstance()

    override fun getResourceLayout(): Int {
        return R.layout.activity_event;
    }

    override fun onResume() {
        super.onResume()

        onGoingSize = 0
        for(i in 0..formsDatas.size - 1){
            if(DatabaseUtils.checkFormIsCompleted(DataSingleton.instance.currentSubId, formsDatas.get(i).id)){
                onGoingSize++
            }
        }

        val titleOnGoing = getString(R.string.label_tab_ongoing, DataSingleton.instance.formList.size - onGoingSize);
        val titleCompleted = getString(R.string.label_tab_completed, onGoingSize);
        tabLayout.getTabAt(0)!!.setText(titleOnGoing)
        tabLayout.getTabAt(1)!!.setText(titleCompleted)
    }

    override fun initData(){
        formsDatas = DataSingleton.instance.formList;

        for(i in 0..formsDatas.size - 1){
            if(DatabaseUtils.checkFormIsCompleted(DataSingleton.instance.currentSubId, formsDatas.get(i).id)){
                onGoingSize++
            }
        }
    }

    override fun initViews() {

        initData();

        val toolBar = findViewById<Toolbar>(R.id.tool_bar);
        setSupportActionBar(toolBar);
        supportActionBar!!.setDisplayHomeAsUpEnabled(true);
        supportActionBar!!.setDisplayShowHomeEnabled(true);

        title = DataSingleton.instance.evetList.get(DataSingleton.instance.currentEventIndex).name;//intent.extras.getString("event_name");
        toolBar.title = title;

        supportActionBar!!.setTitle(title);
        supportActionBar!!.title = title;

        val eventPagerAdapter = EventPagerAdapter(supportFragmentManager, this)
        val viewPager = findViewById<ViewPager?>(R.id.pager_container)
        viewPager!!.adapter = eventPagerAdapter

        tvDate = findViewById<TextView>(R.id.tv_study_date);
        tvDate.text = HelpUtils.getCurrentDate()
        rlEventDate = findViewById<RelativeLayout>(R.id.rl_event_date);

        tabLayout = findViewById<View>(R.id.tabs) as TabLayout
        tabLayout.setupWithViewPager(viewPager)

        // set icons
        val titleOnGoing = getString(R.string.label_tab_ongoing, DataSingleton.instance.formList.size - onGoingSize);
        val titleCompleted = getString(R.string.label_tab_completed, onGoingSize);
        tabLayout.getTabAt(0)!!.setText(titleOnGoing)
        tabLayout.getTabAt(1)!!.setText(titleCompleted)

        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                   dayOfMonth: Int) {
                val dateValue = HelpUtils.getDateValue(cal, year, monthOfYear, dayOfMonth)
                tvDate.text = dateValue
            }
        }

        rlEventDate.setOnClickListener{
            DatePickerDialog(this,
                    dateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()
        }
    }

    override fun onClick(p0: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item!!.getItemId()
        if (id == android.R.id.home) {

            finish();
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    class EventPagerAdapter(fm: FragmentManager, private val context: Context) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return FormsFragment.newInstance(position)
        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return null

        }
    }
}