package net.clinicaldatas.activity

import android.Manifest
import android.app.AlertDialog
import android.os.Bundle
import net.clinicaldatas.R
import android.content.pm.PackageManager
import android.os.Handler
import android.util.Base64
import android.util.Log
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import android.content.Intent
import android.os.Build
import android.view.View
import android.widget.Toast
import net.clinicaldatas.retrofit.response.ApiInterface
import net.clinicaldatas.retrofit.response.LoginResponse
import io.vrinda.kotlinpermissions.PermissionCallBack
import io.vrinda.kotlinpermissions.PermissionsActivity
import kotlinx.android.synthetic.main.activity_login.*
import net.clinicaldatas.utils.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by doba on 1/18/18.
 */
class SplasActivity : PermissionsActivity{

    private val TAG = "SplashActivity"
    constructor() : super()

    private var wait: Long = 500;
    private var mHandler: Handler? = null

    fun initViews() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, object : PermissionCallBack {
                override fun permissionGranted() {
                    super.permissionGranted()
                    loadMainView()
                }

                override fun permissionDenied() {
                    super.permissionDenied()
                }
            })
        }else{
            loadMainView()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash)

        initViews()

        try {
            val info = packageManager.getPackageInfo(
                    applicationContext.packageName,
                    PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        }
    }

    private fun loadMainView() {

        var isLogined = PreferenceUtils.getBooleanPref(this, PrefConstant.PREF_LOGIN, false)
        if(isLogined){
            var username = PreferenceUtils.getStringPref(this, PrefConstant.PREF_USERNAME, "")
            var pass = PreferenceUtils.getStringPref(this, PrefConstant.PREF_PASS, "")

            if(HelpUtils.isOnline(this)){
                callWebService(username, pass)
            }else{
                val simpleAlert = AlertDialog.Builder(this).create()
                simpleAlert.setTitle("Network problem")
                simpleAlert.setMessage("Please turn on your network")

                simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", {
                    dialogInterface, i ->
                    val intent = Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS)
                    startActivityForResult(intent, 100)
                })
                simpleAlert.setButton(AlertDialog.BUTTON_NEGATIVE, "No", {
                    dialogInterface, i -> finish()
                })

                simpleAlert.show()
            }
        }else{
            mHandler = Handler();
            mHandler!!.postDelayed(Runnable {
                enterLoginView()
            }, wait);
        }
    }

    fun callWebService(username : String, password: String) {

        PreferenceUtils.saveStringPref(this, PrefConstant.PREF_USERNAME, username)
        PreferenceUtils.saveStringPref(this, PrefConstant.PREF_PASS, password)

        DataSingleton.instance.username = username;
        DataSingleton.instance.password = password;
        val apiService = ApiInterface.createService(ApiInterface::class.java, username, password)
        val call = apiService.login()
        call.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>?) {
                if (response != null) {
                    if(response.code() == 200){

                        var loginResponse = response.body() as LoginResponse;

                        DataSingleton.instance.trackedEntityAttributes = loginResponse.userCredentials.userRoles.get(0).programs.get(0).programTrackedEntityAttributes;
                        DataSingleton.instance.programId = loginResponse.userCredentials.userRoles.get(0).programs.get(0).id;
                        DataSingleton.instance.organizations = loginResponse.organisationUnits;

                        val intent = Intent(baseContext, MainActivity::class.java);
                        startActivity(intent);
                        finish()
                    }else{
                        enterLoginView()
                    }

                }else{
                    enterLoginView()
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                enterLoginView()
            }
        })
    }

    fun enterLoginView(){
        val intent = Intent(baseContext, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 100){
            loadMainView()
        }
    }
}