package net.clinicaldatas.activity

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import net.clinicaldatas.R
import android.view.animation.AnimationSet
import android.view.animation.AlphaAnimation
import android.view.animation.DecelerateInterpolator
import android.view.animation.Interpolator
import android.widget.ImageView
import android.widget.Toast
import net.clinicaldatas.BuildConfig
import net.clinicaldatas.retrofit.response.ApiInterface
import net.clinicaldatas.retrofit.response.LoginResponse
import net.clinicaldatas.utils.*
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by doba on 1/19/18.
 */
class LoginActivity : BaseActivity{

    private var TAG = "LoginActivity";
    constructor() : super()

    private lateinit var butLogin : Button;

    override fun getResourceLayout(): Int {
        return R.layout.activity_login;
    }

    override fun initViews() {
        butLogin = findViewById<Button>(R.id.but_login)
        butLogin.setOnClickListener{
            login()
        }

        val fadeIn = AlphaAnimation(0f, 1f)
        fadeIn.interpolator = DecelerateInterpolator() as Interpolator? //add this
        fadeIn.duration = 3000

        val animation = AnimationSet(false) //change to false
        animation.addAnimation(fadeIn)

        val imgBanner = findViewById<ImageView>(R.id.img_banner);
        imgBanner.animation = animation;

        if(BuildConfig.DEBUG){
            edt_username.setText(APIConstant.TEST_USERNAME);
            edt_pass.setText(APIConstant.TEST_PASS);
        }
    }

    override fun initData() {
    }

//    fun initDatas(){
//        val gson = Gson()
//
//        // Get the json stream from quiz_json raw file
//        val jsonStream = InputStreamReader(resources.openRawResource(R.raw.program_metadata))
//
//// Create a buffered reader
//        val bufferedReader = BufferedReader(jsonStream)
//
//// Call the fromJson method to get the parsed results
//        val studyList: List<ProgramItem> = gson.fromJson(bufferedReader, Array<ProgramItem>::class.java).toList()
//
//    }

    override fun onClick(p0: View?) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun login(){

        if(HelpUtils.isOnline(baseContext)){
            callWebService(edt_username.text.toString(), edt_pass.text.toString())
        }else{
            val simpleAlert = AlertDialog.Builder(this).create()
            simpleAlert.setTitle("Network problem")
            simpleAlert.setMessage("Please turn on your network")

            simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", {
                dialogInterface, i ->
                val intent = Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS)
                startActivityForResult(intent, 100)
            })
            simpleAlert.setButton(AlertDialog.BUTTON_NEGATIVE, "No", {
                dialogInterface, i -> finish()
            })

            simpleAlert.show()
        }
    }

    fun callWebService(username : String, password: String) {

        PreferenceUtils.saveStringPref(this, PrefConstant.PREF_USERNAME, username)
        PreferenceUtils.saveStringPref(this, PrefConstant.PREF_PASS, password)

        DataSingleton.instance.username = username;
        DataSingleton.instance.password = password;
        showLoading()
        val apiService = ApiInterface.createService(ApiInterface::class.java, username, password)
        val call = apiService.login()
        call.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>?) {
                hideLoading()

                if(response != null){

                    if (response.code() == 200) {

                        PreferenceUtils.saveBooleanPref(baseContext, PrefConstant.PREF_LOGIN, true)
                        var loginResponse = response.body() as LoginResponse;

                        DataSingleton.instance.trackedEntityAttributes = loginResponse.userCredentials.userRoles.get(0).programs.get(0).programTrackedEntityAttributes;
                        DataSingleton.instance.programId = loginResponse.userCredentials.userRoles.get(0).programs.get(0).id;
                        DataSingleton.instance.organizations = loginResponse.organisationUnits;

                        val intent = Intent(baseContext, MainActivity::class.java);
                        startActivity(intent);
                        finish()
                    }else{
                        Toast.makeText(baseContext, "Login fail, please try again", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                hideLoading()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 100){
            login()
        }
    }
}