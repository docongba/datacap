package net.clinicaldatas.activity

import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.View
import android.view.animation.*
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import net.clinicaldatas.R
import net.clinicaldatas.database.DatabaseUtils
import net.clinicaldatas.utils.DataConstant
import net.clinicaldatas.utils.DataSingleton
import net.clinicaldatas.utils.HelpUtils

/**
 * Created by doba on 1/18/18.
 */
class CompleteActivity : BaseActivity {

    private val TAG = "CompleteActivity"
    constructor() : super()
    private var IsFinishEvent : Boolean = false;

    override fun getResourceLayout(): Int {
        return R.layout.activity_complete;
    }

    override fun initViews() {

        val butNextForm  = findViewById<Button>(R.id.but_move_form);

        var currentFormIndex = DataSingleton.instance.currentFormIndex;
        butNextForm.setOnClickListener{
            if(!IsFinishEvent){
                DataSingleton.instance.currentFormIndex = currentFormIndex + 1 ;

                val intent : Intent

                if(DataSingleton.instance.selectedFormList.get(DataSingleton.instance.currentFormIndex).id.equals(HelpUtils.COM_ID)){
                    intent = Intent(baseContext, RiceActivity::class.java);
                }else if(DataSingleton.instance.selectedFormList.get(DataSingleton.instance.currentFormIndex).groupForm){
                    intent = Intent(baseContext, SportFormActivity::class.java);
                }else if(DataSingleton.instance.selectedFormList.get(DataSingleton.instance.currentFormIndex).id.equals(HelpUtils.WEEK_DAY_ID)) {
                    intent = Intent(baseContext, VehicleFormActivity::class.java);
                    intent.putExtra(DataConstant.KEY_FORM_TYPE, DataConstant.FORM_ACTION)
                }else if(DataSingleton.instance.selectedFormList.get(DataSingleton.instance.currentFormIndex).id.equals(HelpUtils.WORKING_DAY_ID)) {
                    intent = Intent(baseContext, VehicleFormActivity::class.java);
                    intent.putExtra(DataConstant.KEY_FORM_TYPE, DataConstant.FORM_VEHICLE)
                }else if(DataSingleton.instance.selectedFormList.get(DataSingleton.instance.currentFormIndex).id.equals(HelpUtils.FOMR_5)){
                    intent = Intent(baseContext, VehicleFormActivity::class.java);
                    intent.putExtra(DataConstant.KEY_FORM_TYPE, DataConstant.FORM_VEHICLE)
                }else{
                    var latestQuestion = DatabaseUtils.getLatestQuestionFormIndex(DataSingleton.instance.currentSubId, DataSingleton.instance.selectedFormList.get(DataSingleton.instance.currentFormIndex).id)

                    intent = Intent(baseContext, QuestionActivity::class.java);
                    intent.putExtra("position", latestQuestion)
                }
                startActivity(intent);
            }
            finish();
        }

        val butBackEvent = findViewById<Button>(R.id.but_back_event);
        butBackEvent.setOnClickListener{
            finish()
        }

        val formLabel = DataSingleton.instance.selectedFormList.get(currentFormIndex).displayName;



        val backEventLabel = DataSingleton.instance.evetList.get(DataSingleton.instance.currentEventIndex).name;
        butBackEvent.text = getString(R.string.label_back_to_event, backEventLabel);

        if(currentFormIndex == DataSingleton.instance.selectedFormList.size - 1){
            butNextForm.visibility = View.GONE
        }else{
            val nextForm = DataSingleton.instance.selectedFormList.get(currentFormIndex + 1).displayName;
            butNextForm.text = getString(R.string.label_move_form, nextForm);
        }

        if(DataSingleton.instance.currentEventIndex == DataSingleton.instance.evetList.size - 1
            && currentFormIndex == DataSingleton.instance.selectedFormList.size - 1){
            IsFinishEvent = true
        }

        if(IsFinishEvent){
            butNextForm.setText("Finish event")
        }

        val tvLabel = findViewById<TextView>(R.id.tv_form_name);
        tvLabel.text = getString(R.string.label_complete, formLabel);

        val imgFinish = findViewById<ImageView>(R.id.img_finsh);
        val tvCongratulation = findViewById<TextView>(R.id.tv_congratulation);

        val animation = AnimationUtils.loadAnimation(applicationContext, R.anim.zoom_out_animation)
        imgFinish.startAnimation(animation)

        tvCongratulation.startAnimation(animation)

        val shake = AnimationUtils.loadAnimation(applicationContext, R.anim.shake_animation)
        tvLabel.startAnimation(shake);


        val fadeIn = AlphaAnimation(0f, 1f)
        fadeIn.interpolator = DecelerateInterpolator() //add this
        fadeIn.duration = 2000
        val animationFade = AnimationSet(false) //change to false
        animationFade.addAnimation(fadeIn)

        butNextForm.animation = animationFade;
        butBackEvent.animation = animationFade;
    }

    override fun initData() {
    }

    override fun onClick(p0: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}