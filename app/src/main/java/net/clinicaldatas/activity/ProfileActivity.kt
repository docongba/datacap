package net.clinicaldatas.activity

import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import com.squareup.picasso.Picasso
import net.clinicaldatas.R
import net.clinicaldatas.utils.PrefConstant
import net.clinicaldatas.utils.PreferenceUtils
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_profile.*

/**
 * Created by doba on 1/21/18.
 */
class ProfileActivity : BaseActivity {

    private var TAG : String = "ProfileActivity";

    constructor() : super()

    override fun getResourceLayout(): Int {
        return R.layout.activity_profile;
    }

    override fun initData(){

    }

    override fun initViews() {
        val toolBar = findViewById<Toolbar>(R.id.tool_bar);
        setSupportActionBar(toolBar);
        supportActionBar!!.setDisplayHomeAsUpEnabled(true);
        supportActionBar!!.setDisplayShowHomeEnabled(true);

        supportActionBar!!.setTitle(R.string.label_profile);

        Picasso.with(this).load("nemo").placeholder(R.color.grey).into(findViewById<CircleImageView>(R.id.img_avatar));

        tv_profile_name.text = PreferenceUtils.getStringPref(this, PrefConstant.PREF_USERNAME, "Nemo")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData();
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onClick(p0: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}