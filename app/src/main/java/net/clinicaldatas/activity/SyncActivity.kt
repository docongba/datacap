package net.clinicaldatas.activity

import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Button
import android.widget.Toast
import net.clinicaldatas.R
import net.clinicaldatas.adapter.SubItemAdapter
import net.clinicaldatas.database.DatabaseUtils
import net.clinicaldatas.database.SubItem
import net.clinicaldatas.retrofit.request.DataValue
import net.clinicaldatas.retrofit.request.Event
import net.clinicaldatas.retrofit.request.SyncPostData
import net.clinicaldatas.retrofit.response.ApiInterface
import net.clinicaldatas.retrofit.response.SyncResponse
import net.clinicaldatas.utils.DataSingleton
import net.clinicaldatas.utils.HelpUtils
import net.clinicaldatas.utils.PrefConstant
import net.clinicaldatas.utils.PreferenceUtils
import kotlinx.android.synthetic.main.activity_sync.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by doba on 1/21/18.
 */
class SyncActivity : BaseActivity {

    private var TAG : String = "SyncActivity";

    constructor() : super()
    private lateinit var subDatas: List<SubItem>;
    private lateinit var adapter : SubItemAdapter;

    override fun getResourceLayout(): Int {
        return R.layout.activity_sync;
    }

    override fun initData(){

        subDatas = DatabaseUtils.getAllSubs() as List<SubItem>
        adapter = SubItemAdapter(this, subDatas){

            DataSingleton.instance.currentSubId = subDatas.get(it).id;

            val intent = Intent(this, EventListActivity::class.java);
            intent.putExtra("study_name", DataSingleton.instance.studyList.get(0).name);
            intent.putExtra("study_date", DataSingleton.instance.studyList.get(0).date);
            intent.putExtra("study_id", DataSingleton.instance.studyList.get(0).id);
            intent.putExtra("study_description", DataSingleton.instance.studyList.get(0).description);
            startActivity(intent);
        }

        var recyclerView = findViewById<RecyclerView>(R.id.rc_sync);
        recyclerView.layoutManager = LinearLayoutManager(baseContext)
        recyclerView.adapter = adapter
    }

    override fun initViews() {

        val toolBar = findViewById<Toolbar>(R.id.tool_bar);
        setSupportActionBar(toolBar);
        supportActionBar!!.setDisplayHomeAsUpEnabled(true);
        supportActionBar!!.setDisplayShowHomeEnabled(true);
        supportActionBar!!.setTitle(R.string.label_sync_server);

        butSync.setOnClickListener {
            butSync.setEnabled(false)
            if(subDatas.size > 0){
                showLoading()
                syncData(subDatas.get(0))
            }else{
                Toast.makeText(baseContext, "Nothing to sync to server", Toast.LENGTH_SHORT).show()
            }
        }

        initData()

        if(subDatas.size == 0){
            butSync.visibility = View.GONE
            tv_sync_label.setText("Nothing to sync to server")
        }
    }

    override fun onClick(p0: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun syncData(subItem: SubItem){
        var questionDatas =  DatabaseUtils.getAllQuestionsBySubId(subItem.id)
        var events = ArrayList<Event>()

        var eventDbOs = DatabaseUtils.getEvents(subItem.id)

        for(i in 0..eventDbOs.size - 1){
            var event = Event()
            event.dueDate = eventDbOs.get(i).dueDate
            event.eventDate = eventDbOs.get(i).eventDate
            event.programStage = eventDbOs.get(i).programStage
            event.status = "ACTIVE"

            var dataValues = ArrayList<DataValue>()
            for (m in 0..questionDatas.size-1){
                if(questionDatas.get(m).eventId.equals(eventDbOs.get(i).programStage)){

                    var dataValue = DataValue()
                    dataValue.value = questionDatas.get(m).value

                    //Used for True/false question
                    if(questionDatas.get(m).isChecked){
                        dataValue.value = "true"
                    }

                    dataValue.rowNum = questionDatas.get(m).rowNum
                    dataValue.dataElement = questionDatas.get(m).questionId
                    dataValues.add(dataValue)
                }
            }

            event.dataValues.addAll(dataValues)
            events.add(event)
        }

        var enrollment = SyncPostData.Enrollment()
        enrollment.status = "ACTIVE"
        enrollment.incidentDate = HelpUtils.getCurrentDate()
        enrollment.enrollmentDate = HelpUtils.getCurrentDate()

        var trackEntity = SyncPostData.TrackedEntityItem()
        trackEntity.trackedEntity = DataSingleton.instance.trackedEntity

        var attributes = ArrayList<SyncPostData.Attribute>()

        var attribute = SyncPostData.Attribute()
            attribute.attribute = DataSingleton.instance.trackedEntityAttributes.get(0).trackedEntityAttribute.id
            attribute.value = subItem.subEnrollId
            attributes.add(attribute)

        if(DataSingleton.instance.trackedEntityAttributes.size > 1){
            attribute = SyncPostData.Attribute()
            attribute.attribute = DataSingleton.instance.trackedEntityAttributes.get(1).trackedEntityAttribute.id
            attribute.value = subItem.subEnrollName
            attributes.add(attribute)
        }

        trackEntity.attributes = attributes

        var syncData  = SyncPostData();
        syncData.orgUnit = DataSingleton.instance.orgUnit
        syncData.program = DataSingleton.instance.studyList.get(0).id
        syncData.enrollment = enrollment
        syncData.events = events
        syncData.trackedEntityInstance = trackEntity

        var username = PreferenceUtils.getStringPref(this, PrefConstant.PREF_USERNAME, "")
        var pass = PreferenceUtils.getStringPref(this, PrefConstant.PREF_PASS, "")

        val apiService = ApiInterface.createService(ApiInterface::class.java, username, pass)
        val call = apiService.postSyncData(syncData)
        call.enqueue(object : Callback<SyncResponse> {
            override fun onResponse(call: Call<SyncResponse>, response: Response<SyncResponse>?) {
                butSync.setEnabled(true)

                if (response != null) {
                    var syncResponse = response.body() as SyncResponse
                    if(syncResponse.httpStatusCode == 200 ){
                        Toast.makeText(baseContext, syncResponse.message, Toast.LENGTH_LONG).show()
                        if(subDatas.isNotEmpty()){
                            subDatas.drop(0)

                            adapter.notifyDataSetChanged()
                            DatabaseUtils.removeSubFromDatabase(subItem.id)

                            if(subDatas.isNotEmpty()){
                                syncData(subDatas.get(0))
                            }else{
                                hideLoading()
                            }
                        }else{
                            hideLoading()
                            DatabaseUtils.deleteDatabase()
                            Toast.makeText(baseContext, syncResponse.message, Toast.LENGTH_LONG).show()
                            adapter.notifyDataSetChanged()
                        }
                    }
                }else{
                    hideLoading()
                }
            }

            override fun onFailure(call: Call<SyncResponse>, t: Throwable) {
                hideLoading()
            }
        })
    }
}