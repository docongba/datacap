package net.clinicaldatas.activity

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import net.clinicaldatas.R
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

/**
 * Created by doba on 1/18/18.
 */
abstract class BaseActivity : AppCompatActivity(), View.OnClickListener{

    public abstract fun getResourceLayout() : Int;
    public abstract fun initViews()
    public abstract fun initData()
    private lateinit var dialog : AlertDialog;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getResourceLayout());
        initViews()
        initData()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    fun showLoading() {
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_loading, null)
        dialogBuilder.setView(dialogView)
        dialog = dialogBuilder.create()

        dialog.show()
    }

    fun hideLoading(){
        if(dialog != null){

            dialog.dismiss();
            dialog.cancel()
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }


    private fun <T> unsafeLazy(initializer: () -> T) = lazy(LazyThreadSafetyMode.NONE, initializer)

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item!!.getItemId()

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {

            finish();
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}