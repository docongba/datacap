package net.clinicaldatas.activity

import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import net.clinicaldatas.R
import net.clinicaldatas.adapter.EventAdapter
import net.clinicaldatas.database.DatabaseUtils
import net.clinicaldatas.models.EventItem
import net.clinicaldatas.utils.DataSingleton
import net.clinicaldatas.utils.HelpUtils

/**
 * Created by doba on 1/21/18.
 */
class EventListActivity : BaseActivity {

    private var TAG : String = "EventListActivity";

    constructor() : super()

    private lateinit var recyclerView : RecyclerView;
    private lateinit var adapter: EventAdapter;
    private lateinit var eventDatas : ArrayList<EventItem>;

    private lateinit var studyName : TextView;
    private lateinit var studyDate : TextView;
    private lateinit var studyDescription : TextView;
    private lateinit var selectedEvent : String;


    override fun getResourceLayout(): Int {
        return R.layout.activity_study;
    }

    override fun initData(){

        DataSingleton.instance.currentStudyId = intent.extras.getString("study_id");

        for (i in 0..(DataSingleton.instance.studyList.size - 1)){
            if(DataSingleton.instance.currentStudyId.equals(DataSingleton.instance.studyList.get(i).id)){
                eventDatas = DataSingleton.instance.studyList.get(i).programStages;
            }
        }

        for(i in 0..eventDatas.size - 1){
            DatabaseUtils.addNewEvent(DataSingleton.instance.currentSubId, eventDatas.get(i).id)
            eventDatas.get(i).subId = DataSingleton.instance.currentSubId
        }

        DataSingleton.instance.evetList = eventDatas;
    }

    override fun initViews() {

        val toolBar = findViewById<Toolbar>(R.id.tool_bar);
        setSupportActionBar(toolBar);
        supportActionBar!!.setDisplayHomeAsUpEnabled(true);
        supportActionBar!!.setDisplayShowHomeEnabled(true);

        val title = intent.extras.getString("subject_name");
        toolBar.title = title;

        studyName = findViewById<TextView>(R.id.tv_study_title);
        studyDate = findViewById<TextView>(R.id.tv_study_date);
        studyDescription = findViewById<TextView>(R.id.tv_study_desc);
        studyDescription.setMovementMethod(ScrollingMovementMethod());

        studyName.text = intent.extras.getString("study_name");
        studyDate.text = HelpUtils.getCurrentDate()
        studyDescription.text = intent.extras.getString("study_description");
        studyDescription.visibility = View.GONE

        supportActionBar!!.setTitle(title);
        supportActionBar!!.title = resources.getString(R.string.label_study);

        recyclerView = findViewById<RecyclerView>(R.id.rc_event_list);
        recyclerView.layoutManager = LinearLayoutManager(baseContext)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapter = EventAdapter(baseContext, intent.extras.getString("study_description"), eventDatas) {
            if(it > 0){

                DataSingleton.instance.currentEventIndex = it-1;
                DataSingleton.instance.formList = DataSingleton.instance.evetList.get(it-1).programStageSections;

                selectedEvent = eventDatas.get(it-1).name;

                for(i in 0..eventDatas.size - 1){
                    eventDatas.get(i).selected = false;
                    if(i == it-1){
                        eventDatas.get(i).selected = true;
                    }
                }

                val intent = Intent(this, FormListActivity::class.java);
                startActivity(intent);
            }

        }

        recyclerView.adapter = adapter;
    }

    override fun onResume() {
        super.onResume()

            adapter.notifyDataSetChanged();
    }

    override fun onClick(p0: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}